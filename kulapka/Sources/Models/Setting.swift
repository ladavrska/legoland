import Foundation

struct Setting: Equatable {
    var localeCode: LocaleCode
    var personSetting: PersonSetting
    var personName: String
    var landingDestination: LandingDestination
}

extension Setting {
    static var mock: Self {
        .init(
            localeCode: .en,
            personSetting: .single,
            personName: "Jack Sparrow",
            landingDestination: .prague
        )
    }
}

enum PersonSetting: String, Equatable, CaseIterable, Identifiable {
    case single
    case group

    var id: String {
        self.rawValue
    }
}

enum LandingDestination: String, Equatable, CaseIterable, Identifiable {
    case prague
    case bratislav
    case zlin
    case bucharest
    case hradecKralove

    var id: String {
        self.rawValue
    }
}
