import SwiftUI

struct BackButtonView: View {

    let onTap: () -> Void

    var body: some View {
        ButtonView(
            text: nil,
            image: Resource.Image.pirate_back.image,
            cornerRadius: 50,
            innerPadding: .init(top: 5, leading: 5, bottom: 5, trailing: 5),
            onTap: onTap
        )
        .frame(width: 64, height: 64)
    }
}

struct BackButton_Previews: PreviewProvider {
    static var previews: some View {
        BackButtonView(onTap: {})
    }
}
