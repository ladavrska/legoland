import Combine
import SwiftUI

public struct ActivityOverlayView: View {

    public enum OverlayState: Equatable {
        case hide
        case loading(_ message: String)
    }

    private let state: OverlayState

    public init(state: OverlayState) {
        self.state = state
    }

    private var isVisible: Bool {
        if case .hide = state {
            return false
        }
        return true
    }

    private var title: String {
        switch state {
        case .hide: return ""
        case .loading(let message): return message
        }
    }

    public var body: some View {
        ZStack(alignment: .bottom) {
            if isVisible {
                Color.black
                    .opacity(0.3)
                    .transition(.opacity)

                HStack(spacing: 4) {
                    ProgressView()
                        .progressViewStyle(CircularProgressViewStyle())

                    TextView(.verbatim(title), size: 25, weight: .bold)
                        .multilineTextAlignment(.leading)
                        .foregroundColor(Theme.Colors.textColor)
                }
                .padding()
                .background(
                    Color
                        .white
                        .frame(minHeight: 62)
                        .frame(minWidth: 500)
                )
                .transition(.opacity)
                .padding(.bottom, 30)
            }
        }
        .animation(.default, value: isVisible)
        .ignoresSafeArea()
        .colorScheme(.light)
    }
}

struct ActivityOverlayView_Previews: PreviewProvider {
    static var previews: some View {
        ActivityOverlayView(
            state: .loading(
                "Loading something....flsjflskfjslkfjsdlkfjsdfjslfjsdsdfjslfdflksskfjsdklfjskdlfjsdfjsdlkfjsklfjsldfkjsldkfjlskdfjsdklfjslkdfjsjkdfslkdfj"
            )
        )
    }
}
