import SwiftUI

public struct CustomNavigationBarView: View {
    var leading: (() -> AnyView)?
    var title: (() -> AnyView)?
    var trailing: (() -> AnyView)?
    var backgroundColor: Color
    var height: CGFloat

    public init(
        leading: (() -> AnyView)? = nil,
        title: TextViewContent,
        trailing: (() -> AnyView)? = nil,
        backgroundColor: Color? = nil,
        height: CGFloat = 65
    ) {
        self.init(
            leading: leading,
            title: {
                AnyView(
                    TextView(
                        title,
                        .largeTitle
                    )
                    .lineLimit(1)
                    .foregroundColor(.white)
                )
            },
            trailing: trailing,
            backgroundColor: backgroundColor,
            height: height
        )
    }

    public init(
        leading: (() -> AnyView)? = nil,
        title: (() -> AnyView)? = nil,
        trailing: (() -> AnyView)? = nil,
        backgroundColor: Color? = nil,
        height: CGFloat = 65
    ) {
        self.leading = leading
        self.trailing = trailing
        self.title = title
        self.backgroundColor = backgroundColor ?? .clear
        self.height = height
    }

    public var body: some View {
        HStack(alignment: .center) {
            if let leading = leading {
                leading()
                Spacer()
            }

            if let title = title {
                HStack(spacing: 0) {
                    Spacer(minLength: 0)

                    title()
                    Spacer(minLength: 0)
                }
            }

            if let trailing = trailing {
                Spacer()
                trailing()
            }
        }
        .padding(.horizontal, 20)
        .padding(.top, 20)
        .frame(maxWidth: .infinity)
        .frame(height: height)
        .background(backgroundColor)
    }
}

#if DEBUG

    struct CustomNavigationBarView_Previews: PreviewProvider {
        static var previews: some View {
            VStack {
                Group {
                    Group {
                        CustomNavigationBarView(
                            leading: { Button("Save", action: {}).toAnyView() }
                        )

                        CustomNavigationBarView(
                            trailing: {
                                Button("Cancel", action: {}).toAnyView()
                            }
                        )

                        CustomNavigationBarView(
                            leading: { Button("Save", action: {}).toAnyView() },
                            trailing: {
                                Button("Cancel", action: {}).toAnyView()
                            }
                        )
                    }

                    Group {
                        CustomNavigationBarView(
                            title: {
                                TextView(
                                    verbatim:
                                        "Remember to use .lineLimit(1) on custom TextView"
                                )
                                .lineLimit(1)
                                .toAnyView()
                            }
                        )

                        CustomNavigationBarView(
                            title: .verbatim("Hello, World!")
                        )

                        CustomNavigationBarView(
                            title: .verbatim(
                                "Hello, really long long long long title here."
                            )
                        )

                        CustomNavigationBarView(
                            leading: { Button("Save", action: {}).toAnyView() },
                            title: .verbatim("Hello, World!")
                        )

                        CustomNavigationBarView(
                            leading: { Button("Save", action: {}).toAnyView() },
                            title: .verbatim(
                                "Hello, really long long long long title here."
                            )
                        )

                        CustomNavigationBarView(
                            title: .verbatim("Hello, World!"),
                            trailing: {
                                Button("Cancel", action: {}).toAnyView()
                            }
                        )

                        CustomNavigationBarView(
                            title: .verbatim(
                                "Hello, really long long long long title here."
                            ),
                            trailing: {
                                Button("Cancel", action: {}).toAnyView()
                            }
                        )

                        CustomNavigationBarView(
                            leading: { Button("Save", action: {}).toAnyView() },
                            title: .verbatim("Hello, World!"),
                            trailing: {
                                Button("Cancel", action: {}).toAnyView()
                            }
                        )

                        CustomNavigationBarView(
                            leading: { Button("Save", action: {}).toAnyView() },
                            title: .verbatim(
                                "Hello, really long long long long title here."
                            ),
                            trailing: {
                                Button("Cancel", action: {}).toAnyView()
                            }
                        )
                    }
                }
            }
            .padding()
            .background(Color.white)
            .frame(width: 400)
            .previewLayout(.sizeThatFits)
        }
    }

#endif
