import SwiftUI

struct RequireImageFocusView: View {

    var rect: CGRect = .screenRect
    private let insetFocus: CGFloat = 70
    @State private var rotationAngle: Angle = .degrees(0)

    var body: some View {
        Color.black
            .mask(
                HoleShapeMask(in: rect)
                    .fill(style: FillStyle(eoFill: true))
            )
            .background(
                Resource.Image.vector.image
                    .resizable()
                    .scaledToFit()
                    .padding(insetFocus - 1)
                    .rotationEffect(rotationAngle)
                    .animation(
                        .linear(duration: 20).repeatForever(),
                        value: rotationAngle
                    )
                    .onAppear {
                        rotationAngle = .degrees(360)
                    }
            )
            .opacity(0.4)
            .ignoresSafeArea()
    }

    func HoleShapeMask(in rect: CGRect) -> Path {
        var shape = Rectangle().path(in: rect)
        shape.addPath(
            Circle().path(in: rect.insetBy(dx: insetFocus, dy: insetFocus))
        )
        return shape
    }
}

struct RequireImageFocusView_Previews: PreviewProvider {
    static var previews: some View {
        RequireImageFocusView(rect: .screenRect)
            .previewInterfaceOrientation(.landscapeLeft)
    }
}
