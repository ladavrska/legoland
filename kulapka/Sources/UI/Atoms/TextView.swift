import SwiftUI

public enum TextViewContent: Hashable {
    case verbatim(String)
    case key(String)

    public static func == (lhs: TextViewContent, rhs: TextViewContent) -> Bool {
        if case .verbatim(let lhsValue) = lhs,
            case .verbatim(let rhsValue) = rhs
        {
            return lhsValue == rhsValue
        } else if case .key(let lhsKey) = lhs, case .key(let rhsKey) = rhs {
            return lhsKey == rhsKey
        }
        return false
    }

    public func hash(into hasher: inout Hasher) {
        switch self {
        case .verbatim(let value):
            hasher.combine(value)
        case .key(let value):
            hasher.combine(value)
        }
    }
}

public enum TextViewStyle: CaseIterable {
    case inherit
    case largeTitle
    case body
    case headline
    case subheadline
    case buttonPrimary
    case buttonLink
    case headingH2
    case headingH3
    case stepsText

    public static let `default`: Self = .body
}

public func TextView(
    verbatim: String,
    _ style: TextViewStyle = .default
) -> Text {
    return TextView(.verbatim(verbatim), style)
}

public func TextView(
    key: String,
    _ style: TextViewStyle = .default
) -> Text {
    return TextView(.key(key), style)
}

public func TextView(
    _ content: TextViewContent,
    _ style: TextViewStyle = .body
) -> Text {
    return applyTextViewStyle(baseText(content), style: style)
}

public func TextView(
    _ content: TextViewContent,
    size: CGFloat,
    weight: Font.Weight
) -> Text {
    return baseText(content)
        .font(Theme.Fonts.mainFont(size))
        .fontWeight(weight)
}

func baseText(_ content: TextViewContent) -> Text {
    switch content {
    case .verbatim(let verbatim):
        return Text(verbatim: verbatim)
    case .key(let key):
        return Text(verbatim: key.localized)
    }
}

func applyTextViewStyle(_ text: Text, style: TextViewStyle) -> Text {
    switch style {
    case .inherit:
        return text
    case .body:
        return
            text
            .font(Theme.Fonts.mainFont(17))
            .fontWeight(.regular)
    case .headline:
        return
            text
            .font(Theme.Fonts.mainFont(17))
            .fontWeight(.bold)
    case .subheadline:
        return
            text
            .font(Theme.Fonts.mainFont(15))
            .fontWeight(.regular)
    case .largeTitle:
        return
            text
            .font(Theme.Fonts.mainFont(34))
            .fontWeight(.bold)
    case .buttonPrimary:
        return
            text
            .font(Theme.Fonts.mainFont(17))
            .fontWeight(.bold)
    case .buttonLink:
        return
            text
            .font(Theme.Fonts.mainFont(16))
            .fontWeight(.regular)
    case .headingH2:
        return
            text
            .font(Theme.Fonts.mainFont(50))
            .fontWeight(.bold)
    case .headingH3:
        return
            text
            .font(Theme.Fonts.mainFont(40))
            .fontWeight(.medium)
    case .stepsText:
        return
            text
            .font(Theme.Fonts.mainFont(20))
            .fontWeight(.regular)
    }
}

#if DEBUG

    struct SwiftUIView_Previews: PreviewProvider {
        static var previews: some View {
            VStack(alignment: .leading, spacing: 16) {
                TextView(verbatim: "Hello, .default!")
                TextView(verbatim: "Hello, .largeTitle!", .largeTitle)
                TextView(verbatim: "Hello, .body!", .body)
                TextView(verbatim: "Hello, .headline!", .headline)
                TextView(verbatim: "Hello, .subheadline!", .subheadline)
                TextView(verbatim: "Hello, .buttonPrimary!", .buttonPrimary)
                TextView(verbatim: "Hello, .buttonLink!", .buttonLink)
            }
            .multilineTextAlignment(.leading)
            .padding()
            .previewLayout(.sizeThatFits)
        }
    }

#endif
