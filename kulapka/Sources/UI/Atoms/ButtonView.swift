import SwiftUI

struct ButtonView: View {

    var text: String? = nil
    var style: TextViewStyle? = nil
    var image: Image?
    let cornerRadius: CGFloat
    var innerPadding: EdgeInsets = .init()
    let onTap: () -> Void

    var body: some View {

        Button {
            onTap()
        } label: {
            HStack(spacing: 10) {
                if let image = image {
                    image
                        .resizable()
                        .renderingMode(.template)
                        .aspectRatio(contentMode: .fit)
                        .foregroundColor(.white)
                }

                if let text = text {
                    TextView(.verbatim(text), style ?? .headingH3)
                        .foregroundColor(.white)
                }
            }
            .padding(innerPadding)
            .background(Theme.Colors.buttonColor)
            .cornerRadius(cornerRadius)
            .shadow(color: Resource.Color.selected.color, radius: 0, x: 4, y: 6)
        }
    }
}

#if DEBUG
    struct ButtonView_Previews: PreviewProvider {
        static var previews: some View {
            ButtonView(
                text: "Start",
                image: Resource.Image.pirate_back.image,
                cornerRadius: 20,
                onTap: {}
            )
            .frame(width: 300, height: 100)
        }
    }
#endif
