import SwiftUI

protocol PickerViewOption: CaseIterable, Identifiable, Equatable
where AllCases: RandomAccessCollection {
    var title: String { get }
    var image: Image? { get }
}

struct PickerView<Option: PickerViewOption>: View {

    let selectedOption: Option
    @Binding var isVisible: Bool
    let onSelect: (Option) -> Void

    var body: some View {
        Button {
            isVisible.toggle()
        } label: {
            HStack(spacing: 10) {
                if let image = selectedOption.image {
                    image
                        .resizable()
                        .scaledToFit()
                        .frame(width: 20, height: 20)
                        .foregroundColor(.white)
                }

                TextView(.verbatim(selectedOption.title))
                    .foregroundColor(.white)
            }
            .padding(20)
            .frame(minWidth: 200)
            .border(Color.white, width: 1)
        }
        .popover(
            isPresented: $isVisible,
            attachmentAnchor: .point(.center),
            arrowEdge: .trailing
        ) {
            ScrollView {
                VStack(spacing: 0) {
                    ForEach(Option.allCases) { option in
                        Divider()
                        Button {
                            onSelect(option)
                            isVisible.toggle()
                        } label: {
                            HStack(spacing: 10) {
                                if let image = option.image {
                                    image
                                        .resizable()
                                        .scaledToFit()
                                        .frame(width: 20, height: 20)
                                        .foregroundColor(.white)
                                }

                                TextView(.verbatim(option.title))
                                    .foregroundColor(.white)

                                Spacer()

                                if selectedOption == option {
                                    Image(systemName: "checkmark")
                                        .resizable()
                                        .scaledToFit()
                                        .frame(width: 20, height: 20)
                                        .foregroundColor(.white)
                                }
                            }
                            .padding(15)
                            .frame(minHeight: 50)
                        }
                    }
                }
            }
            .frame(minWidth: 350, maxWidth: 500)
            .frame(minHeight: 50, maxHeight: 400)
        }
        .padding()
    }
}
