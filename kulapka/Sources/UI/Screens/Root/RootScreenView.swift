import ComposableArchitecture
import SwiftUI

struct RootScreenView: View {

    let store: RootScreenStore

    private var launchStore: OnboardingScreenStore {
        store.scope(
            state: { $0.onboardingState },
            action: RootScreenAction.onboardingScreenAction
        )
    }

    var body: some View {
        WithViewStore(store) { viewStore in
            ARScreenView(store: .mockStore)
//            OnboardingScreenView(store: launchStore)
//                .fullScreenCover(
//                    isPresented: viewStore.binding(
//                        get: { $0.isPresentedAR },
//                        send: RootScreenAction.setVisibleARScreen
//                    ),
//                    onDismiss: nil,
//                    content: {
//                        IfLetStore(
//                            store.scope(
//                                state: { $0.arScreenState },
//                                action: RootScreenAction.arScreenAction
//                            )
//                        ) {
//                            store in
//                            ARScreenView(store: store)
//                        }
//                    }
//                )
        }
    }
}

#if DEBUG
    struct RootScreenView_Previews: PreviewProvider {
        static var previews: some View {
            RootScreenView(store: .mockStore)
        }
    }
#endif
