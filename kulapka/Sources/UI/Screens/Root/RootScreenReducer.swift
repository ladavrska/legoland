import ComposableArchitecture
import SwiftUI

// MARK: - Type-aliases

typealias RootScreenStore = Store<RootScreenState, RootScreenAction>

extension RootScreenStore {
    static var mockStore: Self {
        .init(
            initialState: .init(),
            reducer: rootScreenReducer,
            environment: .mock
        )
    }
}

typealias RootScreenViewStore = ViewStore<RootScreenState, RootScreenAction>
typealias RootScreenReducer = Reducer<
    RootScreenState, RootScreenAction, RootScreenEnvironment
>

// MARK: - State

struct RootScreenState: Equatable {

    var arScreenState: ARScreenState?
    var onboardingState: OnboardingScreenState = .init()

    var isPresentedAR: Bool {
        return arScreenState != nil
    }

    var settings: Setting {
        onboardingState.settingState
    }

}

// MARK: - Actions

enum RootScreenAction {

    case setVisibleARScreen(Bool)

    // Combined actions
    case onboardingScreenAction(OnboardingScreenAction)
    case arScreenAction(ARScreenAction)
}

// MARK: - Environment

var CurrentEnvironment: RootScreenEnvironment = .live

struct RootScreenEnvironment {

    static let live: RootScreenEnvironment = .init(mainQueue: .main)
    static let mock: RootScreenEnvironment = .init(mainQueue: .main)

    var mainQueue: AnySchedulerOf<DispatchQueue>
}

// MARK: - Reducer

let rootScreenReducer = RootScreenReducer.combine([
    onboardingScreenReducer
        .pullback(
            state: \.onboardingState,
            action: /RootScreenAction.onboardingScreenAction,
            environment: {
                OnboardingScreenEnvironment(mainQueue: $0.mainQueue)
            }
        ),
    RootScreenReducer { state, action, _ in
        return .none
    },
    arScreenReducer
        .optional()
        .pullback(
            state: \.arScreenState,
            action: /RootScreenAction.arScreenAction,
            environment: {
                ARScreenEnvironment(mainQueue: $0.mainQueue)
            }
        ),
    .init({ state, action, environment in
        switch action {

        case .setVisibleARScreen(let isVisible):
            state.arScreenState =
                isVisible
                ? .init(
                    avatar: state.onboardingState.avatar ?? .sword_pirate,
                    setting: state.settings
                )
                : nil
            return .none

        // MARK: - Onboarding actions
        case .onboardingScreenAction(.finished):
            return .init(value: .setVisibleARScreen(true))

        case .onboardingScreenAction(_):
            return .none

        // MARK: - AR actions

        case .arScreenAction(.end(let reason)):
            switch reason {
            case .forcedWithCloseButton:
                return .init(value: .setVisibleARScreen(false))
            }
        case .arScreenAction(_):
            return .none
        }
    }),
])
