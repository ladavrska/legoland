import ARKit
import Combine
import RealityKit
import UIKit

class ARViewController: UIViewController {

    // MARK: - Static

    private static let storyboardName = "AR"
    private static let vcIdentifier = "ARVC"

    static func create(viewStore: ARScreenViewStore) -> ARViewController {
        let storyboard = UIStoryboard(name: storyboardName, bundle: .main)
        let vc =
            storyboard.instantiateViewController(withIdentifier: vcIdentifier)
            as! ARViewController
        vc.modalPresentationStyle = .fullScreen
        vc.viewStore = viewStore
        return vc
    }

    // MARK: - IBOutlets

    @IBOutlet private weak var arView: ARView!

    // MARK: - UI elements

    private var coachingOverlayView: ARCoachingOverlayView!

    // MARK: - Private properties

    private let arResourcesGroupName = "AR Resources"

    private var isRenderingModel: Bool = false
    private var viewStore: ARScreenViewStore!
    private var cancelables: Set<AnyCancellable> = Set()

    private var basicSessionConfiguration: ARWorldTrackingConfiguration {
        let configuration = ARWorldTrackingConfiguration()
        configuration.environmentTexturing = .none
        configuration.planeDetection = []
        configuration.wantsHDREnvironmentTextures = false
        configuration.maximumNumberOfTrackedImages = 1
        configuration.sceneReconstruction = .mesh
        return configuration
    }

    // MARK: - Life-cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        checkARSupport()
        setupARView()
        setupSubscriptions()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        runSession(imageDetection: true, objectDetection: true)
        turnOnOffCoachingView(on: true)
    }

    deinit {
        print("[DEINIT] - ARViewController")
    }

    // MARK: - Setups methods

    private func setupARView() {
        arView.session.delegate = self
        arView.renderOptions = [
            .disablePersonOcclusion, .disableMotionBlur,
            .disableDepthOfField, .disableCameraGrain,
        ]
        arView.debugOptions = [
            //.showPhysics
        ]
    }

    private func setupSubscriptions() {

        // NOTE: This publisher is at this moment unused.
        // It is prepared on when we would like to render some items through ARScreenReducer.
        // At this moment the rendering invoke always happens on this screen.
        viewStore
            .renderObjectsSubject
            .receive(on: .main)
            .flatMap {
                objects -> AnyPublisher<
                    (RenderableObject, Result<Entity, Error>), Never
                > in
                let loadEntities = objects.map { object in
                    EntityFactory.createEntity(for: object)
                        .map { (object, $0) }
                }
                return Publishers.MergeMany(loadEntities).eraseToAnyPublisher()
            }
            .sink { [weak self] (object, result) in
                guard let `self` = self else { return }
                switch result {
                case .success(let entity):
                    let anchor = AnchorEntity(world: object.transform)
                    object.configure(anchor: anchor)
                    anchor.addChild(entity)
                    self.arView.scene.anchors.append(anchor)
                case .failure(let error):
                    print(
                        "Failed to render object: \(object.nameForRender), error: \(error.localizedDescription)"
                    )
                }
            }
            .store(in: &cancelables)

        viewStore
            .publisher
            .planeToDetect
            .dropFirst()  // Skip the initial value
            .sink { [weak self] maybePlane in
                if let plane = maybePlane {
                    let planes: ARWorldTrackingConfiguration.PlaneDetection
                    switch plane {
                    case .horizontal: planes = [.horizontal]
                    case .vertical: planes = [.vertical]
                    @unknown default: fatalError()
                    }
                    self?.runSession(
                        imageDetection: false,
                        objectDetection: false,
                        planeDetection: planes
                    )
                } else {
                    self?.runSession(
                        imageDetection: true,
                        objectDetection: true
                    )
                }
            }
            .store(in: &cancelables)
    }

    /// Methods removes or adds the CoachingOverlay.
    ///  Methods is called by observation 'isScanning' property in viewModel.
    /// - Parameter on: Active or Deactivate the coaching overlay
    private func turnOnOffCoachingView(on: Bool) {
        if on {
            let newCoachingView = createCoachingOverlay(
                goal: .tracking,
                delegate: self,
                session: arView.session,
                automatically: true
            )
            coachingOverlayView = newCoachingView
            arView.addSubview(newCoachingView)
            newCoachingView.constraintToParent()
        } else {
            guard let existingOverlay = coachingOverlayView else { return }
            existingOverlay.removeFromSuperview()
            coachingOverlayView = nil
        }
    }

    private func runSession(
        imageDetection: Bool,
        objectDetection: Bool,
        planeDetection: ARWorldTrackingConfiguration.PlaneDetection = []
    ) {
        var configuration = basicSessionConfiguration
        if imageDetection {
            addDetectionImagesConfiguration(to: &configuration)
        }
        if objectDetection {
            addDetectionObjectsConfiguration(to: &configuration)
        }
        configuration.planeDetection = planeDetection
        turnOnOffSession(on: true, configuration: configuration)
    }

    /// Ends the scanning process
    ///  Methods is called by observation 'isScanning' property in viewModel. If you want end scanning process we recommend do it through the mentioned property.
    /// - Parameter on: Turn ON or OFF the scene view session.
    private func turnOnOffSession(
        on: Bool,
        configuration: ARConfiguration? = nil
    ) {
        if on {
            // The application will does not asleep during use
            UIApplication.shared.isIdleTimerDisabled = true

            let configuration = configuration ?? basicSessionConfiguration
            arView.session.run(configuration)
        } else {
            UIApplication.shared.isIdleTimerDisabled = false
            arView.session.run(
                ARWorldTrackingConfiguration(),
                options: [.resetTracking, .removeExistingAnchors]
            )
        }
    }

    private func addDetectionImagesConfiguration(
        to configuration: inout ARWorldTrackingConfiguration
    ) {
        guard
            let trackingImages = ARReferenceImage.referenceImages(
                inGroupNamed: arResourcesGroupName,
                bundle: nil
            )
        else {
            fatalError("Missing expected asset catalog resources.")
        }
        configuration.detectionImages = trackingImages
        configuration.maximumNumberOfTrackedImages = trackingImages.count
        configuration.automaticImageScaleEstimationEnabled = true
    }

    private func addDetectionObjectsConfiguration(
        to configuration: inout ARWorldTrackingConfiguration
    ) {

        guard
            let detectionObjects = ARReferenceObject.referenceObjects(
                inGroupNamed: "AR Objects",
                bundle: nil
            )
        else {
            fatalError("Missing expected asset catalog resources.")
        }
        configuration.detectionObjects = detectionObjects
    }

    // MARK: - Other private methods -

    private func renderModelAndCompleteStepIfRequired(for anchor: ARAnchor) {
        guard !isRenderingModel else { return }
        let renderableItem: RenderableObjectItem
        let worldAnchor: AnchorEntity
        switch anchor {
        // Images
        case let imageAnchor as ARImageAnchor:
            guard let imageToDetect = self.viewStore.imageToDetect,  // 1. Check if we are looking for the image now
                imageToDetect.rawValue == imageAnchor.referenceImage.name  // 2. Check if the image is the one we are looking for
            else { return }
            let item = imageToDetect.renderableItem
            let itemAnchor = AnchorEntity(world: anchor.transform)
            itemAnchor.name = item.nameForRender
            renderableItem = item
            worldAnchor = itemAnchor

        // Objects
        case let objectAnchor as ARObjectAnchor:
            guard let objectToDetect = self.viewStore.objectToDetect,  // 1. Check if we are looking for the object now
                objectToDetect.rawValue == objectAnchor.referenceObject.name  // 2. Check if the object is the one we are looking for
            else { return }
            let item = objectToDetect.renderableItem
            let itemAnchor = AnchorEntity(world: anchor.transform)
            itemAnchor.name = item.nameForRender
            renderableItem = item
            worldAnchor = itemAnchor

        // Planes
        case let planeAnchor as ARPlaneAnchor:
            guard let planeToDetect = self.viewStore.planeToDetect,
                planeAnchor.alignment == planeToDetect,
                case let .requirePlane(params) = self.viewStore.gameState
                    .gameScriptState.currentStep
            else { return }
            guard
                planeAnchor.extent.x > params.game.requiredPlaneSize.width
                    && planeAnchor.extent.z
                        > params.game.requiredPlaneSize.height
            else { return }
            let item = params.game.renderableItem
            let itemAnchor = AnchorEntity(world: anchor.transform)
            itemAnchor.name = item.nameForRender
            renderableItem = item
            worldAnchor = itemAnchor
        default: return
        }
        self.render(item: renderableItem, anchor: worldAnchor)
    }

    private var manager: SceneRenderManaging?

    private func render(item: RenderableObjectItem, anchor: AnchorEntity) {
        isRenderingModel = true
        EntityFactory.createEntity(
            for: item
        )
        .sink { [weak self] result in
            switch result {
            case .failure(let error):
                print(error)

            case .success(let entity):
                switch item {
                case .socialRoomScene:
                    guard let socialRoom = entity as? Experience.SocialRoom
                    //guard let socialRoom = entity as? Experience.LegoFigure
                    else { return }
                    self?.viewStore.send(
                        .gameAction(
                            .runSceneGame(
                                .socialRoom(.init(entity: socialRoom))
                                //.legoFigure(.init(entity: socialRoom))
                            )
                        )
                    )
                    self?.viewStore.send(
                        .gameAction(.gameScriptAction(.nextStep))
                    )
                case .warehouseScene:
                    guard let warehouse = entity as? Experience.Warehouse,
                        let arView = self?.arView
                    else { return }
                    let manager = WarehouseSceneRenderManager.init(
                        warehouse: warehouse,
                        arView: arView
                    )
                    manager.setup()
                    self?.viewStore.send(
                        .gameAction(
                            .runSceneGame(
                                .warehouse(
                                    .init(entity: warehouse, manager: manager)
                                )
                            )
                        )
                    )
                    self?.viewStore.send(
                        .gameAction(.gameScriptAction(.nextStep))
                    )
                case .ITRoomScene:
                    guard let monkeys = entity as? Experience.Monkeys,
                        let arView = self?.arView
                    else { return }
                    let manager = MonkeySceneRenderManager.init(
                        monkeys: monkeys,
                        arView: arView
                    )
                    manager.setup()
                    self?.viewStore.send(
                        .gameAction(
                            .runSceneGame(
                                .itRoom(
                                    .init(entity: monkeys, manager: manager)
                                )
                            )
                        )
                    )
                    self?.viewStore.send(
                        .gameAction(.gameScriptAction(.nextStep))
                    )
                default:
                    self?.viewStore.send(
                        .gameAction(.gameScriptAction(.nextStep))
                    )
                }

                anchor.addChild(entity)
                self?.arView.scene.anchors.append(anchor)
            }
            self?.isRenderingModel = false
        }.store(in: &cancelables)
    }
}

// MARK: ARSessionDelegate

extension ARViewController: ARSessionDelegate {

    func session(_ session: ARSession, didUpdate frame: ARFrame) {

    }

    func session(_ session: ARSession, didAdd anchors: [ARAnchor]) {

        anchors.forEach { [weak self] anchor in
            self?.renderModelAndCompleteStepIfRequired(for: anchor)
        }
    }

    func session(_ session: ARSession, didUpdate anchors: [ARAnchor]) {
        anchors
            .forEach { [weak self] anchor in
                self?.renderModelAndCompleteStepIfRequired(for: anchor)
            }
    }

    func session(_ session: ARSession, didRemove anchors: [ARAnchor]) {

    }
}

extension ARViewController: ARCoachingOverlayViewDelegate {

    func coachingOverlayViewDidRequestSessionReset(
        _ coachingOverlayView: ARCoachingOverlayView
    ) {}

    func coachingOverlayViewWillActivate(
        _ coachingOverlayView: ARCoachingOverlayView
    ) {}

    func coachingOverlayViewDidDeactivate(
        _ coachingOverlayView: ARCoachingOverlayView
    ) {}

}
