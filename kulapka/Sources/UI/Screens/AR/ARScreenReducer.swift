import ARKit
import Combine
import ComposableArchitecture
import RealityKit
import SwiftUI

// MARK: - Type-aliases

typealias ARScreenStore = Store<ARScreenState, ARScreenAction>
extension ARScreenStore {
    static var mockStore: Self {
        .init(
            initialState: .init(avatar: .gun_pirate, setting: .mock),
            reducer: arScreenReducer,
            environment: .mock
        )
    }
}
typealias ARScreenViewStore = ViewStore<ARScreenState, ARScreenAction>
typealias ARScreenReducer = Reducer<
    ARScreenState, ARScreenAction, ARScreenEnvironment
>

// MARK: - State

struct ARScreenState: Equatable {

    enum GrandCameraPermissionState: Equatable {
        case waitingForCameraPermissionResult
        case granted
        case failed(String)
    }

    var grandCameraPermissionState: GrandCameraPermissionState =
        .waitingForCameraPermissionResult
    var activityOverlayState: ActivityOverlayView.OverlayState = .loading(
        Resource.Text.AR.waitingCameraPermission.localized
    )
    let renderObjectsSubject: PassthroughSubject<[RenderableObject], Never> =
        .init()

    // Nested states
    var cameraPermissionState: CameraPermissionState {
        get { .init() }
        set {}
    }

    var gameState: GameState

    var imageToDetect: GameRecognizableImage? {
        switch gameState.gameScriptState.currentStep {
        case .requireImage(let params):
            return params.image
        case .navigation(let params):
            if case let .image(image) = params.confirmBy {
                return image
            } else {
                return nil
            }
        default:
            return nil
        }
    }

    var objectToDetect: GameRecognizableObject? {
        guard
            case let .requireObject(params) = gameState.gameScriptState
                .currentStep
        else {
            return nil
        }
        return params.object
    }

    var planeToDetect: ARPlaneAnchor.Alignment? {
        guard
            case let .requirePlane(params) = gameState.gameScriptState
                .currentStep
        else { return nil }
        return params.plane
    }

    var isRunningGame: Bool {
        return gameState.isRunning
    }

    init(avatar: Avatar, setting: Setting) {
        self.gameState = .init(avatar: avatar, setting: setting)
    }

}

// MARK: - Actions

enum ARScreenAction {

    enum FinishReason: Equatable {
        case forcedWithCloseButton
    }

    case render([RenderableObject])

    // Nested actions
    case cameraPermission(CameraPermissionAction)
    case gameAction(GameAction)
    case end(FinishReason)

}

// MARK: - Environment

struct ARScreenEnvironment {
    static let mock: Self = .init(mainQueue: .main)

    var mainQueue: AnySchedulerOf<DispatchQueue>
}

// MARK: - Reducer

let arScreenReducer =
    ARScreenReducer
    .combine([
        cameraPermissionReducer
            .pullback(
                state: \.cameraPermissionState,
                action: /ARScreenAction.cameraPermission,
                environment: {
                    CameraPermissionEnvironment(mainQueue: $0.mainQueue)
                }
            ),
        gameReducer
            .pullback(
                state: \.gameState,
                action: /ARScreenAction.gameAction,
                environment: {
                    GameEnvironment(mainQueue: $0.mainQueue)
                }
            ),
        ARScreenReducer { state, action, environment in
            switch action {

            case .render(let objects):
                state.renderObjectsSubject.send(objects)
                return .none

            // MARK: - Camera permission
            case .cameraPermission(.cameraPermissionGrantedResult(.success(_))):
                state.activityOverlayState = .hide
                state.grandCameraPermissionState = .granted
                return .none

            case .cameraPermission(
                .cameraPermissionGrantedResult(.failure(let error))
            ):
                state.activityOverlayState = .hide
                state.grandCameraPermissionState = .failed(
                    error.localizedDescription
                )
                return .none

            case .cameraPermission(_):
                return .none

            case .end(_):
                return .none

            // MARK: - Game
            case .gameAction(_):
                return .none
            }
        },
    ])
