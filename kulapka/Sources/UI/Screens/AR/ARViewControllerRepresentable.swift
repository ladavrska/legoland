import SwiftUI

struct ARViewControllerRepresentable: UIViewControllerRepresentable {
    let viewStore: ARScreenViewStore

    func makeUIViewController(context: Context) -> ARViewController {
        return ARViewController.create(viewStore: viewStore)
    }

    func updateUIViewController(
        _ uiViewController: ARViewController,
        context: Context
    ) {
    }
}
