import ARKit
import RealityKit
import UIKit

protocol HasRenderableObjectItem {
    var renderableItem: RenderableObjectItem { get }
}

enum RenderableObjectItem: RenderableObject {

    case printerScene
    case foundImageScene
    case earthObject
    case bookObject
    case warehouseScene
    case socialRoomScene
    case ITRoomScene

    var id: UUID {
        UUID()
    }

    var transform: simd_float4x4 {
        .init()
    }

    var nameForRender: String {
        switch self {
        case .printerScene: return "PrinterScene: \(id)"
        case .foundImageScene: return "FoundImageScene: \(id)"
        case .earthObject: return "Earth: \(id)"
        case .bookObject: return "Book: \(id)"
        case .warehouseScene: return "Warehouse: \(id)"
        case .socialRoomScene: return "SocialRoom: \(id)"
        case .ITRoomScene: return "IT Room: \(id)"
        }
    }
}

public protocol RenderableObject {
    var id: UUID { get }
    var transform: simd_float4x4 { get }
    var nameForRender: String { get }
}

extension RenderableObject {
    /// If for the object is created his own anchor and the anchor is added to the world, the properties specified for the object has to be applied for the anchor.
    func configure(anchor: AnchorEntity) {
        anchor.name = nameForRender
        anchor.transform = Transform(matrix: transform)
    }
}
