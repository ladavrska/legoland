import SwiftUI

struct LaunchAROverlayView: View {

    @State private var isVisible: Bool = true

    var body: some View {
        ZStack {
            VStack(spacing: 0) {
                if isVisible {

                    Resource.Image.background.image
                        .resizable()
                        .scaledToFill()
                        .transition(.move(edge: .top))

                    VStack(spacing: 0) {

                        Color
                            .white
                            .frame(height: 1)

                        Resource.Image.background.image
                            .resizable()
                            .scaledToFill()

                    }
                    .transition(.move(edge: .bottom))
                    .onAppear {
                        DispatchQueue.main.asyncAfter(
                            deadline: .now() + .seconds(2)
                        ) {
                            isVisible = false
                        }
                    }
                }
            }
            .animation(.default, value: isVisible)

            if isVisible {
                HStack(spacing: 4) {

                    ProgressView()
                        .progressViewStyle(CircularProgressViewStyle())
                        .colorScheme(.light)
                        .padding(.trailing, 15)

                    TextView(
                        .verbatim(Resource.Text.AR.settingUpAR.localized),
                        size: 25,
                        weight: .bold
                    )
                    .multilineTextAlignment(.leading)
                    .foregroundColor(Theme.Colors.textColor)
                }
                .padding()
                .background(
                    Color
                        .white
                        .frame(minHeight: 62)
                        .frame(minWidth: 500)
                )
            }
        }
        .animation(.default, value: isVisible)

    }
}

struct LaunchAROverlayView_Previews: PreviewProvider {
    static var previews: some View {
        LaunchAROverlayView()
    }
}
