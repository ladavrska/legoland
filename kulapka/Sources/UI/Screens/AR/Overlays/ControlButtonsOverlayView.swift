import ComposableArchitecture
import SwiftUI

struct ControlButtonsOverlayView: View {

    let store: ARScreenStore

    var body: some View {

        WithViewStore(store) { viewStore in
            if viewStore.isRunningGame {
                VStack {
                    HStack(spacing: 0) {
                        Button {
                            viewStore.send(.end(.forcedWithCloseButton))
                        } label: {
                            closeButton(viewStore: viewStore)
                        }

                        Spacer()
                    }
                    .padding([.leading, .top], 25)
                    Spacer()
                }
            }
        }
    }

    @ViewBuilder
    func closeButton(viewStore: ARScreenViewStore) -> some View {
        Button(
            action: {
                withAnimation {
                    viewStore.send(.end(.forcedWithCloseButton))
                }
            },
            label: {
                Image(systemName: "xmark")
                    .padding(.all, 15)
                    .font(.title3)
                    .foregroundColor(.white)
                    .frame(width: 50, height: 50)
                    .background(
                        VisualEffectView(effect: UIBlurEffect(style: .dark))
                            .cornerRadius(5)
                    )
            }
        )
        .frame(width: 50, height: 50)
    }
}

#if DEBUG
    struct ControlButtonsOverlayView_Previews: PreviewProvider {
        static var previews: some View {
            ControlButtonsOverlayView(store: .mockStore)
        }
    }

#endif
