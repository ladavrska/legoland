import ComposableArchitecture
import SwiftUI

struct ARScreenView: View {

    let store: ARScreenStore

    var gameStore: Store<GameState, GameAction> {
        self.store.scope(state: \.gameState, action: ARScreenAction.gameAction)
    }

    var body: some View {
        WithViewStore(store) { viewStore in
            switch viewStore.grandCameraPermissionState {
            case .waitingForCameraPermissionResult:
                askingForCameraPermissionBackground(viewStore: viewStore)
            case .granted:
                arScreen(viewStore: viewStore)
            case .failed(let message):
                TextView(.verbatim(message), .subheadline)
            }
        }
        .ignoresSafeArea()
    }

    @ViewBuilder
    private func askingForCameraPermissionBackground(
        viewStore: ARScreenViewStore
    ) -> some View {

        Resource.Image.background.image
            .resizable()
            .scaledToFill()
            .overlay(ActivityOverlayView(state: viewStore.activityOverlayState))
            .ignoresSafeArea()
            .onAppear {
                viewStore.send(.cameraPermission(.askForCameraPermissions))
            }
    }

    @ViewBuilder
    private func arScreen(viewStore: ARScreenViewStore) -> some View {
        ARViewControllerRepresentable(viewStore: viewStore)
            .overlay(LaunchAROverlayView())
            .overlay(GameView(store: gameStore))
            //.overlay(ControlButtonsOverlayView(store: store))
            .ignoresSafeArea()
            .onAppear {
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3)) {
                    viewStore.send(.gameAction(.gameScriptAction(.startGame)))
                }
            }
    }
}

#if DEBUG
    struct ARScreenView_Previews: PreviewProvider {
        static var previews: some View {
            ARScreenView(store: .mockStore)
        }
    }
#endif
