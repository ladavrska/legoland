import AVFoundation
import Combine
import ComposableArchitecture
import SwiftUI

// MARK: - Type-aliases

typealias CameraPermissionStore = Store<
    CameraPermissionState, CameraPermissionAction
>

extension CameraPermissionStore {
    static var mockStore: Self {
        .init(
            initialState: .init(),
            reducer: cameraPermissionReducer,
            environment: .mock
        )
    }
}

typealias CameraPermissionViewStore = ViewStore<
    CameraPermissionState, CameraPermissionAction
>
typealias CameraPermissionReducer = Reducer<
    CameraPermissionState, CameraPermissionAction, CameraPermissionEnvironment
>

// MARK: - State

struct CameraPermissionState: Equatable {

}

// MARK: - Actions

enum CameraPermissionAction {
    case cameraPermissionGrantedResult(Result<Void, Error>)
    case askForCameraPermissions
}

// MARK: - Environment

struct CameraPermissionEnvironment {
    static let mock: Self = .init(mainQueue: .main)

    var mainQueue: AnySchedulerOf<DispatchQueue>
}

// MARK: - Reducer

let cameraPermissionReducer = CameraPermissionReducer {
    state,
    action,
    environment in

    enum CameraPermissionError: LocalizedError {
        case cameraPermissionDenied
        case noCamera
        case unknownCaseWithCameraPermission

        public var errorDescription: String? {
            switch self {
            case .noCamera: return nil
            case .cameraPermissionDenied:
                return
                    "The app cannot run the AR session because the camera permission was denied. You can grant the permission in the application settings."
            case .unknownCaseWithCameraPermission: return nil
            }
        }
    }

    switch action {
    case .askForCameraPermissions:
        return Future<Void, Error> { promise in
            switch AVCaptureDevice.authorizationStatus(for: .video) {
            case .authorized:  // The user has previously granted access to the
                promise(.success(()))
            case .notDetermined:  // The user has not yet been asked for camera access.
                AVCaptureDevice.requestAccess(for: .video) { granted in
                    if granted {
                        promise(.success(()))
                    } else {
                        promise(
                            .failure(
                                CameraPermissionError.cameraPermissionDenied
                            )
                        )
                    }
                }
            case .denied:  // The user has previously denied access.
                promise(.failure(CameraPermissionError.cameraPermissionDenied))
            case .restricted:  // The user can't grant access due to restrictions.
                promise(.failure(CameraPermissionError.noCamera))
            default:
                promise(
                    .failure(
                        CameraPermissionError.unknownCaseWithCameraPermission
                    )
                )
            }
        }
        .receive(on: DispatchQueue.main)
        .catchToEffect()
        .map(CameraPermissionAction.cameraPermissionGrantedResult)
    case .cameraPermissionGrantedResult(.success(_)):

        return .none

    case .cameraPermissionGrantedResult(.failure(_)):
        // TODO: Handle this situation
        return .none
    }
}
