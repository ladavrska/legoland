import ComposableArchitecture
import SwiftUI

struct OnboardingSettingsOverlayView: View {

    let store: OnboardingScreenStore

    var settingsScreenStore: SettingScreenStore {
        return store.scope(
            state: { $0.settingScreenState },
            action: OnboardingScreenAction.settingScreenAction
        )
    }

    var body: some View {
        WithViewStore(store) { viewStore in
            VStack(alignment: .trailing, spacing: 30) {

                NavigationLink(
                    unwrap: viewStore.binding(
                        get: \.route,
                        send: OnboardingScreenAction.present
                    ),
                    case: /OnboardingScreenRoute.settings,
                    onNavigate: { isActive in
                        setRouteActive(
                            isActive: isActive,
                            route: .settings,
                            viewStore: viewStore
                        )
                    }
                ) { _ in
                    SettingScreenView(store: settingsScreenStore)
                } label: {
                    settingsButton()
                }

                settingsPreviewView(viewStore: viewStore)

            }
            .padding(24)
            .padding(.top, 20)  //Extra padding of status bar
        }
    }

    @ViewBuilder
    private func settingsPreviewView(viewStore: OnboardingScreenViewStore)
        -> some View
    {
        VStack(alignment: .trailing, spacing: 7) {
            TextView(.verbatim(viewStore.settingState.localeCode.title))
            HStack {
                viewStore.settingState.personSetting.image
                TextView(.verbatim(viewStore.personName))
            }
            HStack {
                viewStore.settingState.landingDestination.image
                TextView(
                    .verbatim(viewStore.settingState.landingDestination.title)
                )
            }
        }
    }

    @ViewBuilder
    private func settingsButton() -> some View {
        Resource.Image.wheel_ship_sea.image
            .resizable()
            .renderingMode(.template)
            .aspectRatio(contentMode: .fit)
            .foregroundColor(.white)
            .padding(5)
            .background(Theme.Colors.buttonColor)
            .clipShape(Circle())
            .shadow(color: Resource.Color.selected.color, radius: 0, x: 4, y: 6)
            .frame(width: 64, height: 64)
    }

    private func setRouteActive(
        isActive: Bool,
        route: OnboardingScreenRoute,
        viewStore: OnboardingScreenViewStore
    ) {
        viewStore.send(.present(isActive ? route : nil))
    }
}

struct OnboardingSettingsOverlayView_Previews: PreviewProvider {
    static var previews: some View {
        OnboardingSettingsOverlayView(store: .mockStore)
    }
}
