import ComposableArchitecture
import SwiftUI

enum Avatar: String, CaseIterable, Identifiable {
    var id: String {
        self.rawValue
    }

    case sword_pirate
    case gun_pirate
    case pirate_costume_woman
    case sailor_sea_captain
    case captain_pirate_hat

    var image: Image {
        switch self {
        case .sword_pirate: return Resource.Image.sword_pirate_saber.image
        case .gun_pirate: return Resource.Image.gun_pirate_pistol.image
        case .pirate_costume_woman:
            return Resource.Image.pirate_costume_woman.image
        case .sailor_sea_captain: return Resource.Image.sailor_sea_captain.image
        case .captain_pirate_hat: return Resource.Image.captain_pirate_hat.image
        }
    }
}

struct OnboardingAvatarPickerScreenView: View {

    let store: OnboardingScreenStore

    var body: some View {
        WithViewStore(store) { viewStore in
            ZStack {
                Resource.Image.background.image
                    .resizable()
                    .scaledToFill()

                VStack(spacing: 18) {
                    TextView(
                        .verbatim(
                            String(
                                format: Resource.Text.Onboarding.greeting
                                    .localized,
                                viewStore.personName
                            )
                        ),
                        .headingH2
                    )
                    .foregroundColor(Theme.Colors.textColor)
                    TextView(
                        .verbatim(
                            Resource.Text.Onboarding.selectAvatar.localized
                        ),
                        .headingH2
                    )
                    .foregroundColor(Theme.Colors.textColor)

                    HStack(spacing: 24) {
                        ForEach(Avatar.allCases) { avatar in
                            Button {
                                viewStore.send(.setAvatar(avatar))
                                viewStore.send(.present(.startARScreen))
                            } label: {
                                avatar.image
                                    .resizable()
                                    .renderingMode(.template)
                                    .aspectRatio(contentMode: .fit)
                                    .frame(width: 65, height: 65)
                                    .foregroundColor(Theme.Colors.imageColor)
                                    .padding(20)
                                    .background(.white)
                                    .clipShape(Circle())
                            }
                        }
                    }
                }
            }
            .overlay(
                NavigationLink(
                    unwrap: viewStore.binding(
                        get: \.route,
                        send: OnboardingScreenAction.present
                    ),
                    case: /OnboardingScreenRoute.startARScreen,
                    onNavigate: { isActive in
                        viewStore.send(
                            .present(isActive ? .startARScreen : nil)
                        )
                    }
                ) { _ in
                    OnboardingStartARScreenView(store: store)
                } label: {
                    EmptyView()
                }
            )
        }
        .overlay(
            OnboardingSettingsOverlayView(store: store),
            alignment: .topTrailing
        )
        .hideNavigationBar()
    }
}

struct OnboardingAvatarPickerScreenView_Previews: PreviewProvider {
    static var previews: some View {
        OnboardingAvatarPickerScreenView(store: .mockStore)
            .previewInterfaceOrientation(.landscapeLeft)
    }
}
