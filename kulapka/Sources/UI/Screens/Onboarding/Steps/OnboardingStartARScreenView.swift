import ComposableArchitecture
import SwiftUI

struct OnboardingStartARScreenView: View {

    let store: OnboardingScreenStore

    var body: some View {
        WithViewStore(store) { viewStore in
            ZStack {
                Resource.Image.background.image
                    .resizable()
                    .scaledToFill()

                VStack(spacing: 70) {

                    VStack(spacing: 20) {
                        TextView(
                            .verbatim(
                                Resource.Text.Onboarding.avatarCompliment
                                    .localized
                            ),
                            .headingH3
                        )
                        .foregroundColor(Theme.Colors.textColor)

                        viewStore.avatar?.image
                            .resizable()
                            .renderingMode(.template)
                            .aspectRatio(contentMode: .fit)
                            .frame(width: 106, height: 122)
                            .foregroundColor(.white)
                            .padding()
                            .background(Theme.Colors.imageColor.opacity(0.6))
                            .clipShape(Circle())

                    }

                    VStack(spacing: 30) {
                        TextView(
                            .verbatim(
                                Resource.Text.Onboarding.readyToPlay.localized
                            ),
                            .headingH2
                        )
                        .foregroundColor(Theme.Colors.textColor)

                        ButtonView(
                            text: Resource.Text.Onboarding.startButtonTitle
                                .localized,
                            style: .headingH2,
                            cornerRadius: 8,
                            innerPadding: .init(
                                top: 12,
                                leading: 48,
                                bottom: 12,
                                trailing: 48
                            )
                        ) {
                            viewStore.send(.finished)
                        }
                    }
                }
            }
            .overlay(
                backButton(viewStore: viewStore),
                alignment: .topLeading
            )
        }
        .hideNavigationBar()
        .ignoresSafeArea()
    }

    @ViewBuilder
    private func backButton(viewStore: OnboardingScreenViewStore) -> some View {
        BackButtonView {
            viewStore.send(.present(nil))
        }
        .padding(24)
        .padding(.top, 20)  //Extra padding of status bar
    }

    @ViewBuilder
    private func startButtonLabel() -> some View {
        Label {
            TextView(
                .verbatim(Resource.Text.Onboarding.startButtonTitle.localized),
                .buttonPrimary
            )
        } icon: {
            Resource.Image.ar_glyph_dark.image
        }
        .padding(.horizontal, 20)
        .background(Color.white)
        .cornerRadius(10)
        .foregroundColor(.black)
    }
}

struct OnboardingStartARScreenView_Previews: PreviewProvider {
    static var previews: some View {
        OnboardingStartARScreenView(store: .mockStore)
            .previewInterfaceOrientation(.landscapeLeft)
    }
}
