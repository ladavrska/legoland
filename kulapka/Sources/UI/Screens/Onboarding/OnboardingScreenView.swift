import ComposableArchitecture
import SwiftUI

struct OnboardingScreenView: View {

    let store: OnboardingScreenStore

    var settingsScreenStore: SettingScreenStore {
        return store.scope(
            state: { $0.settingScreenState },
            action: OnboardingScreenAction.settingScreenAction
        )
    }

    var body: some View {
        WithViewStore(store) { viewStore in

            NavigationView {
                OnboardingAvatarPickerScreenView(store: store)
            }
            .navigationViewStyle(StackNavigationViewStyle())
        }
    }
}

#if DEBUG
    struct LaunchScreenView_Previews: PreviewProvider {
        static var previews: some View {
            OnboardingScreenView(store: .mockStore)
        }
    }
#endif

struct HideNavigationBarModifier: ViewModifier {
    func body(content: Content) -> some View {
        content
            .navigationBarBackButtonHidden(true)
            .navigationBarHidden(true)
            .navigationBarTitle("")
    }
}

extension View {
    func hideNavigationBar() -> some View {
        modifier(HideNavigationBarModifier())
    }
}
