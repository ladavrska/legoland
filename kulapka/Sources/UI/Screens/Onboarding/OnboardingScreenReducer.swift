import ComposableArchitecture
import SwiftUI

// MARK: - Type-aliases

typealias OnboardingScreenStore = Store<
    OnboardingScreenState, OnboardingScreenAction
>

extension OnboardingScreenStore {
    static var mockStore: Self {
        .init(
            initialState: .init(avatar: .sailor_sea_captain),
            reducer: onboardingScreenReducer,
            environment: .mock
        )
    }
}

typealias OnboardingScreenViewStore = ViewStore<
    OnboardingScreenState, OnboardingScreenAction
>
typealias OnboardingScreenReducer = Reducer<
    OnboardingScreenState, OnboardingScreenAction, OnboardingScreenEnvironment
>
typealias OnboardingScreenRoute = OnboardingScreenState.Route

// MARK: - State

struct OnboardingScreenState: Equatable {

    /// Navigation route. The route will be pushed to the navigation stack.
    enum Route: Equatable {
        case settings
        case startARScreen
    }

    var route: Route?
    var settingState: Setting = .init(
        localeCode: Localization.localeCode,
        personSetting: .single,
        personName: "",
        landingDestination: .prague
    )
    var avatar: Avatar?

    var settingScreenState: SettingScreenState {
        get {
            return .init(
                localCode: settingState.localeCode,
                personSetting: settingState.personSetting,
                landingDestination: settingState.landingDestination,
                personName: settingState.personName
            )
        }
        set {
            settingState.localeCode = newValue.localCode
            settingState.personSetting = newValue.personSetting
            settingState.personName = newValue.personName
            settingState.landingDestination = newValue.landingDestination
        }
    }

    var personName: String {
        settingState.personName.isEmpty
            ? "Jack Sparrow" : settingState.personName
    }
}

// MARK: - Actions

enum OnboardingScreenAction {
    case present(OnboardingScreenRoute?)

    case settingScreenAction(SettingScreenAction)
    case setAvatar(Avatar?)
    case finished
}

// MARK: - Environment

struct OnboardingScreenEnvironment {
    static let mock: Self = .init(mainQueue: .main)

    var mainQueue: AnySchedulerOf<DispatchQueue>
}

// MARK: - Reducer

let onboardingScreenReducer = OnboardingScreenReducer.combine([

    settingScreenReducer
        .pullback(
            state: \.settingScreenState,
            action: /OnboardingScreenAction.settingScreenAction,
            environment: {
                .init(mainQueue: $0.mainQueue)
            }
        ),
    OnboardingScreenReducer { state, action, environment in

        switch action {
        case .present(let route):
            state.route = route
            return .none

        case .setAvatar(let maybeAvatar):
            state.avatar = maybeAvatar
            return .none

        case .finished:
            print("finished")
            return .none

        // MARK: - Setting screen action

        case .settingScreenAction(_):
            return .none
        }
    },
])
