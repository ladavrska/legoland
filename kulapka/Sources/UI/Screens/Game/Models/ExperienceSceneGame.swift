import Foundation
import RealityKit

enum ExperienceSceneGameType {
    case socialRoom
    case warehouse
    case itRoom
}

enum ExperienceSceneGame: Equatable {
//    case legoFigure(LegoFigureSceneGameParameters)
    case socialRoom(SocialRoomSceneGameParameters)
    case warehouse(WarehouseSceneGameParameters)
    case itRoom(ITRoomSceneGameParameters)
}

//struct LegoFigureSceneGameParameters: Equatable {
//    let entity: Experience.LegoFigure
//}

struct SocialRoomSceneGameParameters: Equatable {
    let entity: Experience.SocialRoom
}

struct WarehouseSceneGameParameters: Equatable {
    let entity: Experience.Warehouse
    let manager: WarehouseSceneRenderManager
}

struct ITRoomSceneGameParameters: Equatable {
    let entity: Experience.Monkeys
    let manager: MonkeySceneRenderManager
}
