import ARKit
import Foundation
import RealityKit
import SwiftUI

typealias GameScript = [GameStory: [GameStoryStep]]

class HardcodedGameScriptProvider {

    typealias Text = Resource.Text.StoryText

    private let setting: Setting
    private var persons: PersonSetting {
        setting.personSetting
    }

    var script: GameScript {
        [
            .bigMeetingRoom: [
//                .question(
//                    .init(
//                        question:
//                            "You are in the social room, great! What do you see in front of you?",
//                        options: [
//                            .right("Coffee machine"),
//                            .right("Dishwasher"),
//                            .wrong("Big flower"),
//                        ]
//                    )
//                ),
//                .question(
//                    .init(
//                        question: "In which floor is the accounting?",
//                        options: [
//                            .wrong("Foor 1"),
//                            .right("Floor 2"),
//                            .wrong("Floor 3"),
//                        ]
//                    )
//                ),
//                .storyTelling(.init(text: Text.legend(persons).localized)),
                .confirmation(
                    .init(
                        text: Text.lookForBottle(persons).localized,
                        buttonText: "OK",
                        image: .init(
                            resource: .pirate_bottle,
                            size: .big,
                            layout: .leading
                        )
                    )
                ),
                .requireImage(.init(image: .bottle)),
                .confirmation(
                    .init(
                        text:
                            Text.foundBottle(persons).localized,
                        buttonText: Text.foundBottleConfirm.localized,
                        image: .init(
                            resource: .pirate_map,
                            size: .big,
                            layout: .middle
                        )
                    )
                ),
                .navigation(
                    .init(
                        instruction: Text.forDrinkGoFloor1.localized,
                        map: .smecky_floor0_go_floor1,
                        confirmBy: .button(
                            Text.forDrinkGoFloor1Confirm.localized
                        )
                    )
                ),
                .navigation(
                    .init(
                        instruction: Text.forDrinkGoSocialRoom.localized,
                        map: .smecky_floor1_go_social,
                        confirmBy: .image(.drink)
                    )
                ),
                .gratulationFinishedStory(
                    .init(
                        gratulationText: Text.BigMeetingRoom
                            .gratulationFinishedStoryTitle.localized,
                        nextInstructionText: Text.BigMeetingRoom
                            .gratulationFinishedStoryDescription.localized,
                        buttonText: Text.BigMeetingRoom
                            .gratulationFinishedStoryButtonTitle.localized
                    )
                ),
            ],
            .socialRoom: [
                .confirmation(
                    .init(
                        text: Text.SocialRoom.firstInstructionsText.localized,
                        buttonText: Text.SocialRoom.firstInstructionsButtonTitle
                            .localized,
                        image: .init(
                            resource: .pirate_bartender,
                            size: .medium,
                            layout: .top
                        )
                    )
                ),
                .requirePlane(
                    .init(plane: .horizontal, game: .socialRoom)
                ),
                .requireFinishSceneGame(.socialRoom),
                .gratulationFinishedStory(
                    .init(
                        gratulationText: Text.SocialRoom
                            .gratulationFinishedSceneTitle.localized,
                        nextInstructionText: Text.SocialRoom
                            .gratulationFinishedSceneDescription.localized,
                        buttonText: Text.SocialRoom
                            .gratulationFinishedSceneButtonTitle.localized
                    )
                ),
                .navigation(
                    .init(
                        instruction: "Find reception and scan the image",
                        map: .smeckyLevel0,
                        confirmBy: .image(.reception)
                    )
                ),

            ],
            .reception: [
                .requirePlane(
                    .init(plane: .horizontal, game: .warehouse)
                ),
                .requireFinishSceneGame(.warehouse),
                .requireObject(
                    .init(
                        instruction:
                            "A prosím tě, tady ještě děláme nový motiv naší pirátské vlajky, prosím mohl bys ho támhle na naší kopírce naskenovat a poslat mailem na recepcepraha@cngroup.dk. ",
                        object: .printer
                    )
                ),
                // 4. Ve chvíli kdy načte hráč kopírku, tak mu na tabletu vyskočí display kopírky aby se tím proklikal ke správnému postupu.
                // 5. Jakmile úspěšně odešle vlajku,  naskočí zpátky dívka z recepce.
                // “díky moc za pomoc, kdybys potřeboval pomoc třeba s rozbitou židlí, nebo nahlásit nefungující kávovar, tak prosím k nám na tento email! A ted si běž vyzvednout vedle na účtárnu měšec zlatáků jako zálohu na tvoje putování a po cestě se zastav v našem skladišti, vem si tužku a papír, budou se ti hodit na cestu”
                // 6. Načíst obrázek tužky a papíru na skříni (poznámka, když už jsi tady, můžeš si tady vzít, co potřebuješ).
                // 7. Naskočí mapka s účtárnou a měšcem jako záchytným bodem. (.navigationStep)
                // 8. Jakmile hráč dojde na účtárnu, načte obrázek měšce, objeví se velký stůl, s kalamářem, brkem, spoustou svitků a počítadlem. A tam Nada: “ Kampak kampak plavčíku? Rychle co potřebuješ, mám spoustu práce s výplatama! Jo ty jsi ten nováček a přišel jsis pro zálohu. Ale to není jen tak. Abych věděla, že jsi ten správný, musíš mi říct: “něco z onboardingu” // .requireImage(with game)
                // 9. Require finish game (quiz)
                // Po zakliknutí správné odpovědi se zhmotní měšec. Tady ho máš, ale nezapomen mi ho vyuúčtovat v rámci cestáku.
                // A kdybys někdy oženil, pořídil si děti, přestěhoval se, nezapomen mi to nahlásit! A ted běž za Benem do zbrojnice, má pro tebe připravené nějaké nádobíčko, co ti pomůže s hledáním pokladu.
            ],
            .itRoom: [
                .navigation(
                    .init(
                        instruction: Text.forITSymbolGoITRoom.localized,
                        map: .smecky_floor0_go_floor1,
                        confirmBy: .button(
                            Text.forITSymbolConfirm.localized
                        )
                    )
                ),
                .requireImage(.init(image: .ITSymbol)),
                .confirmation(
                    .init(
                        text:
                            Text.foundITSymbol(persons).localized,
                        buttonText: Text.foundITSymbolConfirm.localized,
                        image: .init(
                            resource: .pirate_map,
                            size: .big,
                            layout: .middle
                        )
                    )
                ),
                .requirePlane(
                    .init(plane: .horizontal, game: .itRoom)
                ),
                .requireFinishSceneGame(.itRoom),
                .gratulationFinishedStory(
                    .init(
                        gratulationText: Text.ITRoom
                            .gratulationFinishedSceneTitle.localized,
                        nextInstructionText: Text.ITRoom
                            .gratulationFinishedSceneDescription.localized,
                        buttonText: Text.ITRoom
                            .gratulationFinishedSceneButtonTitle.localized
                    )
                ),
                .navigation(
                    .init(
                        instruction: "Find Port Fenix and scan the image",
                        map: .smeckyLevel1,
                        confirmBy: .image(.reception)
                    )
                ),
            ],

        ]
    }

    init(setting: Setting) {
        self.setting = setting
    }

    static func getStep(in script: GameScript, storyIndex: Int, stepIndex: Int)
        -> GameStoryStep?
    {
        guard let story = GameStory(rawValue: storyIndex) else { return nil }
        return script[story]?[safeIndex: stepIndex]
    }
}

enum GameStory: Int, Equatable, Identifiable {
    // Velká zasedačka - start
    case bigMeetingRoom = 0
    case socialRoom = 1
    case reception = 2
    case itRoom = 3

    var title: String {
        switch self {
        case .bigMeetingRoom:
            return Resource.Text.StoryText.storyBigMeetingRoomName.localized
        case .socialRoom:
            return Resource.Text.StoryText.storySocialRoomName.localized
        case .reception: return "Recepce [CZ]"
        case .itRoom: return "IT [CZ]"
        }
    }

    var id: Int {
        return self.rawValue
    }
}

extension CGSize {
    static let medium = Self.init(width: 150, height: 150)
    static let small = Self.init(width: 100, height: 100)
    static let big = Self.init(width: 200, height: 200)
}
