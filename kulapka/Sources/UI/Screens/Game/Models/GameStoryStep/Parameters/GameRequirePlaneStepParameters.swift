import ARKit
import Foundation

struct GameRequirePlaneStepParameters: Equatable, Identifiable {
    let id = UUID()
    let plane: ARPlaneAnchor.Alignment
    /// If on the plane should be rendered some game scene
    let game: PlaneGameScene

    /// The game which supposed to be played on some ARPlaneAnchor.
    enum PlaneGameScene: HasRenderableObjectItem {
        case warehouse
        case socialRoom
        case itRoom

        var renderableItem: RenderableObjectItem {
            switch self {
            case .warehouse: return .warehouseScene
            case .socialRoom: return .socialRoomScene
            case .itRoom: return .ITRoomScene
            }
        }

        /// In meters
        var requiredPlaneSize: SceneSize {
            switch self {
            case .warehouse: return .init(width: 2, height: 2)
            case .socialRoom: return .init(width: 0.5, height: 0.5)
            case .itRoom: return .init(width: 2, height: 2)
            }
        }

        struct SceneSize {
            let width: Float
            let height: Float
        }
    }
}
