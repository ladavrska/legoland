import SwiftUI

// MARK: - Navigation step

struct GameNavigationStepParameters: Equatable, Identifiable {
    let id = UUID()
    let instruction: String
    let map: GameMapImage
    let confirmBy: GameNavigationConfirmationOption
}

enum GameMapImage: Equatable {
    case none
    case smeckyLevel0
    case smeckyLevel1
    case smecky_floor1_go_social
    case smecky_floor2_go_floor1
    case smecky_floor0_go_floor1

    var mapImage: Image {
        switch self {
        case .none: return Resource.Image.nomap.image
        case .smeckyLevel0: return Resource.Image.smeckyLevel0.image
        case .smeckyLevel1: return Resource.Image.smeckyLevel1.image
        case .smecky_floor1_go_social:
            return Resource.Image.smecky_floor1_go_social.image
        case .smecky_floor2_go_floor1:
            return Resource.Image.smecky_floor2_go_floor1.image
        case .smecky_floor0_go_floor1:
            return Resource.Image.smecky_floor0_go_floor1.image
        }
    }
}

enum GameNavigationConfirmationOption: Equatable {
    case button(String)
    case image(GameRecognizableImage)
}
