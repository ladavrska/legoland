//
//  GameQuestionStepParameters.swift
//  kulapka
//
//  Created by Martin Matějka on 26.01.2022.
//

import Foundation

struct GameQuestionStepParameters: Equatable {

    let question: String
    let options: [QuestionOption]
}

enum QuestionOption: Equatable {
    case right(String)
    case wrong(String)
}
