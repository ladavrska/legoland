import Foundation

struct GameGratulationFinishedStepParameters: Equatable {
    let gratulationText: String
    let nextInstructionText: String
    let buttonText: String
}
