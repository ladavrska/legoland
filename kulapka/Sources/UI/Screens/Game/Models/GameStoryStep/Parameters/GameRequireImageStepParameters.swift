import Foundation
import simd

// MARK: - Require image step

struct GameRequireImageStepParameters: Equatable, Identifiable {
    let id = UUID()
    let image: GameRecognizableImage
}

enum GameRecognizableImage: String, Equatable, HasRenderableObjectItem {

    case reception
    case earth
    case books
    case bottle
    case drink
    case ITSymbol

    var imageResource: Resource.Image {
        switch self {
        case .reception: return Resource.Image.reception
        case .earth: return Resource.Image.earth
        case .books: return Resource.Image.book
        case .bottle: return Resource.Image.ar_bottle
        case .drink: return Resource.Image.ar_drink
        case .ITSymbol: return Resource.Image.itroom
        }
    }

    var renderableItem: RenderableObjectItem {
        switch self {
        case .reception: return .foundImageScene
        case .earth: return .earthObject
        case .books: return .bookObject
        case .bottle: return .foundImageScene
        case .drink: return .foundImageScene
        case .ITSymbol: return .foundImageScene
        }
    }

    var title: String {
        switch self {
        case .books: return Resource.Text.AR.RecognizableImage.books.localized
        case .reception:
            return Resource.Text.AR.RecognizableImage.reception.localized
        case .earth: return Resource.Text.AR.RecognizableImage.earth.localized
        case .bottle: return Resource.Text.AR.RecognizableImage.bottle.localized
        case .drink: return Resource.Text.AR.RecognizableImage.drink.localized
        case .ITSymbol:
            return Resource.Text.AR.RecognizableImage.itroom.localized
        }
    }
}
