import Foundation

// MARK: - Story telling step

struct GameStoryTellingStepParameters: Equatable, Identifiable {
    let id = UUID()
    let text: String
}
