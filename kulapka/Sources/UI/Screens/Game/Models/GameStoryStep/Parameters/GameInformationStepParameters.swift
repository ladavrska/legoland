import CoreGraphics
import Foundation

// MARK: - Confirmation step

struct GameInformationStepParameters: Equatable, Identifiable {

    struct Image: Equatable {

        enum Layout: Equatable {
            case top
            case bottom
            case leading
            case trailing
            case middle
        }

        let resource: Resource.Image
        let size: CGSize
        let layout: Layout
    }

    let id = UUID()
    let text: String
    let buttonText: String
    let image: Image?

    init(text: String, buttonText: String, image: Image? = nil) {
        self.text = text
        self.buttonText = buttonText
        self.image = image
    }

}
