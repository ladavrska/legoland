import Foundation
import simd

// MARK: - Require object step

struct GameRequireObjectStepParameters: Equatable, Identifiable {
    let id = UUID()
    let instruction: String
    let object: GameRecognizableObject
}

enum GameRecognizableObject: String, Equatable, HasRenderableObjectItem {

    typealias Text = Resource.Text.AR.RecognizableObject

    case printer
    case coffeeMachine

    var title: String {
        switch self {
        case .printer: return Text.printer.localized
        case .coffeeMachine: return Text.coffeeMachine.localized
        }
    }

    var renderableItem: RenderableObjectItem {
        switch self {
        case .printer: return .printerScene
        case .coffeeMachine: return .socialRoomScene
        }
    }

    var previewImage: Resource.Image {
        switch self {
        case .printer: return .printer_preview
        case .coffeeMachine: return .printer_preview
        }
    }
}
