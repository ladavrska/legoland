import Foundation

// MARK: Story step

enum GameStoryStep: Equatable {
    case confirmation(GameInformationStepParameters)
    case requireImage(GameRequireImageStepParameters)
    case navigation(GameNavigationStepParameters)
    case requireObject(GameRequireObjectStepParameters)
    case storyTelling(GameStoryTellingStepParameters)
    case requirePlane(GameRequirePlaneStepParameters)
    case gratulationFinishedStory(GameGratulationFinishedStepParameters)
    case requireFinishSceneGame(ExperienceSceneGameType)
    case question(GameQuestionStepParameters)
}
