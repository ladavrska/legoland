import ARKit
import Combine
import CoreGraphics
import Foundation
import RealityKit

class WarehouseSceneRenderManager: SceneRenderManaging, Equatable {
    static func == (
        lhs: WarehouseSceneRenderManager,
        rhs: WarehouseSceneRenderManager
    ) -> Bool {
        return true
    }

    static var mock: WarehouseSceneRenderManager {
        .init(warehouse: .init(), arView: .init())
    }

    enum Event {
        case finished
        case checkBoxes
    }

    public var eventsPublisher: AnyPublisher<Event, Never> {
        eventsSubject.eraseToAnyPublisher()
    }

    public var boxesToCollectPublisher: AnyPublisher<Int, Never> {
        boxesToCollectSubject.eraseToAnyPublisher()
    }

    var boxesToCollect: Int {
        boxesToCollectSubject.value
    }

    let arView: ARView

    // Private variables
    private var grabbedEntity: Entity?
    private var handTrigger: Entity?

    // Private constants
    private let cameraAnchor: AnchorEntity
    private let warehouse: Experience.Warehouse
    private let targetEntity: ModelEntity

    private var handGestureState: UIGestureRecognizer.State = .ended
    private var boxesToCollectSubject: CurrentValueSubject<Int, Never>
    private var eventsSubject: PassthroughSubject<Event, Never> = .init()
    private var collisionCancelables: Set<AnyCancellable> = Set()
    private var cancelables: Set<AnyCancellable> = Set()

    init(warehouse: Experience.Warehouse, arView: ARView) {
        self.arView = arView
        self.warehouse = warehouse
        self.targetEntity =
            warehouse.target?.children.first as? ModelEntity ?? .init()
        self.boxesToCollectSubject = .init(warehouse.boxEntities.count)
        self.cameraAnchor = AnchorEntity(.camera)
    }

    // MARK: - Public methods

    /// Needs to be called before it is added to the reality
    func setup() {
        setupCollisionsSubscriptions()
        setupTargetEntity()
        setupBoxes()
        setupHandButton()
        setupSubscriptions()
    }

    // MARK: - Setup methods

    private func setupSubscriptions() {
        eventsPublisher
            .filter { $0 == .checkBoxes }
            .debounce(for: .seconds(2), scheduler: .main)
            .filter { [weak self] _ in
                guard let `self` = self else { return false }
                return self.handGestureState == .ended
            }
            .sink { [weak self] _ in
                self?.checkBoxesAndFinish()
            }
            .store(in: &cancelables)
    }

    private func setupBoxes() {
        warehouse.boxEntities.forEach { box in
            let parentEntity = box.wrapped()
            warehouse.addChild(parentEntity)
        }
    }

    private func setupCollisionsSubscriptions() {

        // NOTE: Note used
        collisionUpdatePublisher
            .sink { _ in }
            .store(in: &collisionCancelables)

        collisionEndedPublisher
            .sink { [weak self] event in
                guard let `self` = self,
                    event.entityA == self.targetEntity
                        && (event.entityB != self.warehouse.hand
                            || event.entityA != self.warehouse.hand)
                else { return }
                self.boxesToCollectSubject.send(
                    self.boxesToCollectSubject.value + 1
                )
            }
            .store(in: &collisionCancelables)

        collisionBeganPublisher
            .sink { [weak self] event in
                guard let `self` = self,
                    event.entityA == self.targetEntity
                        && (event.entityB != self.warehouse.hand
                            || event.entityA != self.warehouse.hand)
                else { return }
                self.boxesToCollectSubject.send(
                    self.boxesToCollectSubject.value - 1
                )
            }
            .store(in: &collisionCancelables)
    }

    private func setupTargetEntity() {
        targetEntity.generateCollisionShapes(recursive: true)
        targetEntity.collision?.mode = .trigger
        targetEntity.collision?.filter = .sensor
        var simpleMaterial = SimpleMaterial()
        simpleMaterial.metallic = MaterialScalarParameter(floatLiteral: 0.9)
        simpleMaterial.roughness = MaterialScalarParameter(floatLiteral: 0.1)
        let mesh: MeshResource = .generateBox(size: 2.5)
        let component = ModelComponent(
            mesh: mesh,
            materials: [simpleMaterial]
        )
        targetEntity.components.set(component)
    }

    private func setupHandButton() {
        guard let hand = warehouse.hand else { return }
        hand.generateCollisionShapes(recursive: true)
        cameraAnchor.addChild(hand)
        addAnchor(anchor: cameraAnchor)
        hand.transform.translation = [0.5, -0.4, -1]
        if let collisionHandTrigger = hand as? (HasCollision & HasPhysicsBody) {
            // should be done differently but for the sake of how engine
            // works is a good practice
            var physics = PhysicsBodyComponent(
                massProperties: .default,
                material: .default,
                mode: .kinematic
            )
            physics.isTranslationLocked = (x: true, y: true, z: true)
            collisionHandTrigger.components[PhysicsBodyComponent.self] = physics
            self.installHandGesture(for: collisionHandTrigger)
            self.handTrigger = hand
        }
    }

    // MARK: - Other private methods

    private func checkBoxesAndFinish() {
        guard boxesToCollect == 0 else { return }
        cameraAnchor.removeFromParent()
        collisionCancelables.forEach { $0.cancel() }
        collisionCancelables.removeAll()
        eventsSubject.send(.finished)
    }

    private func installHandGesture(for hand: HasCollision) {
        installGestures(for: hand)
            .forEach { [weak self] gesture in
                guard let `self` = self else { return }
                gesture.addTarget(
                    self,
                    action: #selector(self.handleHandGesture(_:))
                )
            }
    }

    @objc func handleHandGesture(_ recognizer: UIGestureRecognizer) {
        guard
            let translationRecognizer = recognizer
                as? EntityTranslationGestureRecognizer
        else {
            return
        }
        self.handGestureState = translationRecognizer.state
        switch translationRecognizer.state {
        case .began:
            let entities = arView.entities(at: arView.center)
            guard entities.count > 0, let grabEntity = entities.first,
                grabEntity.name == "parent-box"
            else { return }
            grabEntity.components.set(Physics.Body.Kinematic.component)
            grabEntity.setParent(cameraAnchor, preservingWorldTransform: true)
            self.grabbedEntity = grabEntity
        case .changed:
            guard
                let grabEntityPosition = self.grabbedEntity?.position(
                    relativeTo: self.warehouse
                ),
                let result = rayCast(
                    origin: grabEntityPosition,
                    direction: [0, -1, 0],
                    length: 5.0
                )
            else { return }
            warehouse.spot?.position = SIMD3<Float>(
                x: result.position.x,
                y: 0,
                z: result.position.z
            )
        case .ended:
            guard let grabbedEntity = self.grabbedEntity else { return }
            grabbedEntity.components.set(Physics.Body.Dynamic.component)
            eventsSubject.send(.checkBoxes)
        default: break
        }
    }
}
