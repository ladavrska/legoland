import ARKit
import Combine
import CoreGraphics
import Foundation
import RealityKit

class MonkeySceneRenderManager: SceneRenderManaging, Equatable {
    static func == (
        lhs: MonkeySceneRenderManager,
        rhs: MonkeySceneRenderManager
    ) -> Bool {
        return true
    }

    static var mock: MonkeySceneRenderManager {
        .init(monkeys: .init(), arView: .init())
    }

    enum Event {
        case finished
        case checkMonkeys
    }

    public var eventsPublisher: AnyPublisher<Event, Never> {
        eventsSubject.eraseToAnyPublisher()
    }

    public var monkeysToShootPublisher: AnyPublisher<Int, Never> {
        monkeysToShootSubject.eraseToAnyPublisher()
    }

    var monkeysToShoot: Int {
        monkeysToShootSubject.value
    }

    let arView: ARView

    // Private variables
    private var gun: Entity?
    private var shootTrigger: Entity?

    // Private constants
    private let cameraAnchor: AnchorEntity

    private let monkeys: Experience.Monkeys

    private var monkeysToShootSubject: CurrentValueSubject<Int, Never>

    private var eventsSubject: PassthroughSubject<Event, Never> = .init()
    private var collisionCancelables: Set<AnyCancellable> = Set()
    private var cancelables: Set<AnyCancellable> = Set()

    init(monkeys: Experience.Monkeys, arView: ARView) {
        self.arView = arView
        self.monkeys = monkeys
        self.monkeysToShootSubject = .init(monkeys.monkeyEntities.count)
        self.cameraAnchor = AnchorEntity(.camera)
    }

    // MARK: - Public methods

    /// Needs to be called before it is added to the reality
    func setup() {
        //setupCollisionsSubscriptions()
        setupMonkeys()
        setupHUD()
        setupSubscriptions()
    }

    // MARK: - Setup methods

    private func setupSubscriptions() {
        eventsPublisher
            .filter { $0 == .checkMonkeys }
            .debounce(for: .seconds(2), scheduler: .main)
            .sink { [weak self] _ in
                self?.checkIfCompleted()
            }
            .store(in: &cancelables)
    }

    private func setupMonkeys() {

        self.monkeys.monkeySleep?.isEnabled = false

        self.monkeys.monkeyEntities.forEach({ monkey in
            let parentEntity = monkey.wrapped(tag: "monkey")
            self.monkeys.addChild(parentEntity)

            DispatchQueue.main.async {
                let root =
                    parentEntity.findEntity(named: "_rootJoint") as! ModelEntity
                root.playAnimation(
                    root.availableAnimations[0].repeat(duration: .infinity),
                    transitionDuration: 1.25,
                    startsPaused: false
                )
            }
        })
    }

    func handleTapOnEntity(_ entity: Entity?) {
        showGunfire()
        let entities = arView.entities(at: arView.center)
        if let affectedEntity = entities.first, affectedEntity.name == "monkey"
        {
            killed(monkey: affectedEntity)
        }
    }

    private func killed(monkey: Entity) {
        if let monkeySleepEntity = self.monkeys.monkeySleep?.clone(
            recursive: true
        ) {
            monkey.isEnabled = false
            monkeySleepEntity.transform = monkey.transform
            self.monkeys.addChild(monkeySleepEntity)
            monkeySleepEntity.isEnabled = true

            DispatchQueue.main.async {
                let root =
                    monkeySleepEntity.findEntity(named: "_rootJoint")
                    as! ModelEntity
                root.playAnimation(
                    root.availableAnimations[0].repeat(duration: .infinity),
                    transitionDuration: 1.25,
                    startsPaused: false
                )
            }

            // Fix with combine
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.25) {
                self.monkeysToShootSubject.send(
                    self.monkeysToShootSubject.value - 1
                )
                self.eventsSubject.send(.checkMonkeys)
            }
        }
    }

    private func showGunfire() {
        if let explosion = self.gun?.findEntity(named: "explosion") {
            explosion.isEnabled = true
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                explosion.isEnabled = false
            }
        }
    }

    private func setupHUD() {
        self.arView.scene.addAnchor(cameraAnchor)

        if let trigger = self.monkeys.shoot {
            self.shootTrigger = trigger
            trigger.generateCollisionShapes(recursive: true)
            cameraAnchor.addChild(trigger)
            trigger.transform.translation = [0.5, -0.3, -1]
        }

        if let gun = self.monkeys.gun {
            self.gun = gun
            cameraAnchor.addChild(gun)
            gun.transform.translation = [0.2, -0.6, -0.7]
            // Due to shitty Reality Composer features I had to switch the PBR material with UnlitMaterial so it does
            // not reacts to lighting
            var unlitMaterial = UnlitMaterial()
            unlitMaterial.baseColor = try! MaterialColorParameter.texture(
                TextureResource.load(named: "pirateGun.png")
            )

            if let nestedWithModelEntity = gun.findEntity(named: "PirateGun001")
            {
                var modelComponent =
                    nestedWithModelEntity.components[ModelComponent]
                    as! ModelComponent
                modelComponent = ModelComponent(
                    mesh: modelComponent.mesh,
                    materials: [unlitMaterial]
                )
                nestedWithModelEntity.components.set(modelComponent)
            }

            gun.findEntity(named: "explosion")?.isEnabled = false
        }

        self.monkeys.actions.shootAction.onAction = handleTapOnEntity(_:)
    }

    // MARK: - Other private methods

    private func checkIfCompleted() {
        guard monkeysToShoot == 0 else { return }
        self.monkeys.removeFromParent()
        self.gun?.removeFromParent()
        self.shootTrigger?.removeFromParent()
        self.eventsSubject.send(.finished)
    }

}
