import ARKit
import Combine
import CoreGraphics
import Foundation
import RealityKit

protocol SceneRenderManaging {
    var arView: ARView { get }
}

extension SceneRenderManaging {

    var collisionBeganPublisher: AnyPublisher<CollisionEvents.Began, Never> {
        arView.scene.publisher(for: CollisionEvents.Began.self)
            .eraseToAnyPublisher()
    }

    var collisionUpdatePublisher: AnyPublisher<CollisionEvents.Updated, Never> {
        arView.scene.publisher(for: CollisionEvents.Updated.self)
            .eraseToAnyPublisher()
    }

    var collisionEndedPublisher: AnyPublisher<CollisionEvents.Ended, Never> {
        arView.scene.publisher(for: CollisionEvents.Ended.self)
            .eraseToAnyPublisher()
    }

    func entities(at point: CGPoint) -> [Entity] {
        arView.entities(at: point)
    }

    func rayCast(origin: SIMD3<Float>, direction: SIMD3<Float>, length: Float)
        -> CollisionCastHit?
    {
        arView.scene.raycast(
            origin: origin,
            direction: direction,
            length: length
        ).first
    }

    func addAnchor(anchor: HasAnchoring) {
        arView.scene.addAnchor(anchor)
    }

    func installGestures(for entity: HasCollision) -> [EntityGestureRecognizer]
    {
        arView.installGestures([.translation, .rotation], for: entity)
    }
}
