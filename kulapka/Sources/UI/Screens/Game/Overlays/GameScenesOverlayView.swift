import ComposableArchitecture
import SwiftUI

struct GameScenesOverlayView: View {

    let store: GameStore

    var body: some View {
        IfLetStore.init(
            store.scope(
                state: { $0.gameSceneState },
                action: GameAction.gameSceneAction
            )
        ) {
            store in
            GameSceneView(store: store)
        }
    }
}

struct GameScenesOverlayView_Previews: PreviewProvider {
    static var previews: some View {
        GameScenesOverlayView(store: .mockStore)
    }
}
