import ComposableArchitecture
import SwiftUI

struct GameStatsOverlayView: View {

    @ObservedObject var compassHeading = CompassHeading()

    let store: GameStore

    var body: some View {
        WithViewStore(store) { viewStore in

            VStack(spacing: 0) {

                if viewStore.isVisibleGameStats {

                    if let story = viewStore.gameScriptState.currentStory {

                        HStack(spacing: 0) {
                            // Placeholder for close button
                            Spacer()
                                .frame(width: 70)

                            TextView(.verbatim(story.title), .headingH2)

                            Spacer()

                        }
                        .padding(.leading, 25)
                        .padding(.top, 15)
                        .transition(.move(edge: .leading))
                    }

                    Spacer()

                    HStack(spacing: 0) {

                        VStack(alignment: .leading, spacing: 0) {

                            Spacer()

                            Button {
                                viewStore.send(.toggleMap)
                            } label: {
                                compassButton(viewStore: viewStore)
                            }

                            viewStore.avatar.image
                                .resizable()
                                .renderingMode(.template)
                                .aspectRatio(contentMode: .fit)
                                .frame(width: 160, height: 160)
                                .foregroundColor(.white)
                                .padding()
                                .background(Theme.Colors.imageColor)
                                .clipShape(Circle())
                                .offset(x: -24, y: 24)
                        }

                        VStack {
                            Spacer()
                            HStack(spacing: -20) {
                                ForEach(
                                    Array(
                                        viewStore.stories.sorted(by: {
                                            $0.rawValue < $1.rawValue
                                        })
                                    )
                                ) {
                                    story in
                                    if let currentStory = viewStore.currentStory
                                    {
                                        let isCurrent = currentStory == story
                                        CoinView(
                                            state: isCurrent
                                                ? .colored
                                                : (story.rawValue
                                                    < currentStory.rawValue
                                                    ? .done : .gray)
                                        )
                                        .zIndex(isCurrent ? 100 : 0)
                                        .offset(y: isCurrent ? -16 : 0)
                                    }
                                }
                            }.offset(x: -28, y: 20)
                        }
                        .transition(.move(edge: .bottom))

                        Spacer()
                    }
                    .transition(.move(edge: .bottom))
                }
            }
            .animation(.default, value: viewStore.isVisibleGameStats)
        }
    }

    @ViewBuilder
    private func compassButton(viewStore: GameViewStore) -> some View {
        ZStack {
            Circle()
                .fill(Color.white)
                .frame(width: 70, height: 70)
            Button(
                action: {
                    withAnimation {
                        viewStore.send(.toggleMap)
                    }
                },
                label: {
                    Image(uiImage: UIImage(named: "compass")!)
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .font(.title3)
                        .foregroundColor(.white)
                        .frame(width: 70, height: 70)
                }
            )
            .frame(width: 70, height: 70)
            .rotationEffect(Angle(degrees: self.compassHeading.degrees - 45.0))
        }
    }
}

struct CoinView: View {

    enum State {
        case colored, gray, done
    }

    let state: State

    var body: some View {
        switch state {
        case .colored:

            Resource.Image.gold_money_coin.image
                .resizable()
                .scaledToFit()
                .background(Theme.Colors.textColor)
                .clipShape(Circle())
                .frame(width: 80, height: 80)

        case .gray:

            Resource.Image.gold_money_coin.image
                .renderingMode(.template)
                .resizable()
                .scaledToFit()
                .foregroundColor(Resource.Color.disabled.color)
                .background(Color.gray)
                .clipShape(Circle())
                .frame(width: 80, height: 80)

        case .done:

            Resource.Image.gold_money_coin.image
                .renderingMode(.template)
                .resizable()
                .scaledToFit()
                .foregroundColor(Resource.Color.disabled_bg.color)
                .background(Color.white)
                .frame(width: 80, height: 80)
                .overlay(
                    ZStack {
                        Color.white.opacity(0.75)
                        Image(systemName: "checkmark").resizable()
                            .foregroundColor(.green).frame(
                                width: 24,
                                height: 16
                            )
                    }
                )
                .clipShape(Circle())
        }
    }

}

struct GameStatsOverlayView_Previews: PreviewProvider {
    static var previews: some View {
        //    GameStatsOverlayView(store: .mockStore)
        CoinView(state: .done)
            .preferredColorScheme(.dark)
            .previewLayout(.sizeThatFits)
    }
}
