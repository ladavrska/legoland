import ComposableArchitecture
import SwiftUI

struct GameStepsOverlayView: View {

    struct ViewState: Equatable {
        var currentStep: GameStoryStep?

        var isVisibleSkipStepButton: Bool {
            guard let currentStep = currentStep else { return false }
            let requireSomething: Bool
            switch currentStep {
            case .requireObject: requireSomething = true
            case .confirmation: requireSomething = false
            case .requireImage: requireSomething = true
            case .navigation(let params):
                switch params.confirmBy {
                case .image: requireSomething = true
                case .button: requireSomething = false
                }
            case .storyTelling: requireSomething = false
            case .requirePlane: requireSomething = true
            case .gratulationFinishedStory: requireSomething = false
            case .requireFinishSceneGame: requireSomething = true
            case .question: requireSomething = true
            }
            return AppConstant.isDebug && requireSomething
        }
    }

    let store: Store<ViewState, GameAction>

    init(store: GameStore) {
        self.store = store.scope(state: {
            .init(currentStep: $0.gameScriptState.currentStep)
        })
    }

    var body: some View {
        WithViewStore(store) { viewStore in

            ZStack {
                if let step = viewStore.currentStep {
                    switch step {
                    case let .confirmation(params):
                        InformationStepView(
                            text: params.text,
                            button: .init(
                                title: params.buttonText,
                                onTap: {
                                    viewStore.send(.gameScriptAction(.nextStep))
                                }
                            ),
                            image: params.image
                        )
                        .id(params.text)
                        .transition(.move(edge: .trailing))
                    case let .requireImage(params):
                        RequiredImageStepView(
                            image: params.image
                        )
                        .id(params.image)
                        .transition(.move(edge: .trailing))
                    case let .navigation(params):
                        NavigationStepView(
                            text: params.instruction,
                            confirmBy: params.confirmBy,
                            onTap: {
                                viewStore.send(.gameScriptAction(.nextStep))
                            }
                        )
                        .id(params.instruction)
                        .transition(.move(edge: .trailing))
                    case let .requireObject(params):

                        RequireObjectStepView(
                            text: params.instruction,
                            object: params.object,
                            image: .init(
                                resource: params.object.previewImage,
                                size: .medium
                            )
                        )
                        .id(params.object)
                        .transition(.move(edge: .trailing))

                    case let .storyTelling(params):
                        StoryTellingStepView(text: params.text) {
                            viewStore.send(.gameScriptAction(.nextStep))
                        }
                        .id(params.text)
                        .transition(.opacity)

                    case let .requirePlane(params):
                        RequirePlaneStepView(
                            width: params.game.requiredPlaneSize.width,
                            height: params.game.requiredPlaneSize.height
                        )
                        .id(params.game)
                        .transition(.move(edge: .trailing))

                    case let .gratulationFinishedStory(params):
                        GratulationFinishedStoryStepView(parameters: params) {
                            viewStore.send(.gameScriptAction(.nextStep))
                        }
                        .id(params.nextInstructionText)
                        .transition(.opacity)

                    case .requireFinishSceneGame(let scene):
                        switch scene {
                        case .warehouse:
                            BlankStepView()
                        case .socialRoom:
                            EmptyView()
                        case .itRoom:
                            BlankStepView()
                        }
                    case .question(let parameters):
                        QuestionStepView(
                            question: parameters.question,
                            options: parameters.options
                        ) {
                            viewStore.send(.gameScriptAction(.nextStep))
                        }
                        .id(parameters.question)
                        .transition(.move(edge: .bottom))
                    }
                }
                skipDebugRequireStepButton(viewStore: viewStore)
            }
            .animation(.default, value: viewStore.currentStep)
        }
    }

    @ViewBuilder
    private func skipDebugRequireStepButton(
        viewStore: ViewStore<GameStepsOverlayView.ViewState, GameAction>
    ) -> some View {
        if viewStore.isVisibleSkipStepButton {
            HStack {
                Spacer()
                VStack {
                    Button {
                        viewStore.send(.gameScriptAction(.nextStep))
                    } label: {
                        Label {
                            TextView(.verbatim("Skip step"), .headingH3)
                                .foregroundColor(Theme.Colors.textColor)
                        } icon: {
                            Image(systemName: "arrowshape.zigzag.right")
                                .resizable()
                                .scaledToFit()
                                .foregroundColor(Theme.Colors.textColor)
                                .frame(width: 40, height: 40)
                        }
                        .padding()
                    }
                    Spacer()
                }
            }
            .transition(.opacity)
        } else {
            EmptyView()
        }
    }
}
