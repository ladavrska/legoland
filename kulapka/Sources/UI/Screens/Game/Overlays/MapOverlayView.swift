import ComposableArchitecture
import SwiftUI

struct MapOverlayView: View {

    let store: GameStore

    var body: some View {
        WithViewStore(store) { viewStore in
            if viewStore.isMapVisible {
                ZStack {
                    Theme.Colors.background
                    if viewStore.gameScriptState.currentLocation != .none {
                        viewStore.gameScriptState.currentLocation.mapImage
                            .resizable()
                            .aspectRatio(contentMode: .fill)
                            .edgesIgnoringSafeArea(.top)
                    } else {
                        Text("No map loaded yet!!!")
                    }
                }.ignoresSafeArea()
                    .transition(.move(edge: .bottom))
            }
        }
    }
}
