import ComposableArchitecture
import SwiftUI

struct GameView: View {

    let store: GameStore

    var body: some View {
        WithViewStore(store) { viewStore in
            ZStack {
                GameStepsOverlayView(store: store)
                GameScenesOverlayView(store: store)
                //MapOverlayView(store: store)
                //GameStatsOverlayView(store: store)
            }
        }
    }
}

struct GameOverlayView_Previews: PreviewProvider {
    static var previews: some View {
        GameView(store: .mockStore)
    }
}
