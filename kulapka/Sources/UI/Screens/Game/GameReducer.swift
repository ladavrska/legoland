import ComposableArchitecture
import SwiftUI

// MARK: - Type-aliases

typealias GameStore = Store<GameState, GameAction>

extension GameStore {
    static var mockStore: Self {
        .init(
            initialState: .init(avatar: .pirate_costume_woman, setting: .mock),
            reducer: gameReducer,
            environment: .mock
        )
    }
}

typealias GameViewStore = ViewStore<GameState, GameAction>
typealias GameReducer = Reducer<GameState, GameAction, GameEnvironment>

// MARK: - State

struct GameState: Equatable {

    var avatar: Avatar
    var gameScriptState: GameScriptState
    var gameSceneState: GameSceneState?

    var isRunning: Bool {
        guard case .running = gameScriptState.progressState else {
            return false
        }
        return true
    }

    var isMapVisible: Bool = false

    var isVisibleGameStats: Bool {
        let isStoryTellingStep: () -> Bool = {
            if case .storyTelling(_) = gameScriptState.currentStep {
                return true
            }
            return false
        }
        return isRunning && !isStoryTellingStep()
    }

    var stories: Dictionary<GameStory, [GameStoryStep]>.Keys {
        gameScriptState.script.keys
    }

    var currentStory: GameStory? {
        gameScriptState.currentStory
    }

    init(avatar: Avatar, setting: Setting) {
        self.avatar = avatar
        self.gameScriptState = .init(setting: setting)
    }
}

// MARK: - Actions

enum GameAction {
    case gameScriptAction(GameScriptAction)
    case runSceneGame(ExperienceSceneGame)
    case gameSceneAction(GameSceneAction)
    case toggleMap
}

// MARK: - Environment

struct GameEnvironment {
    static let mock: Self = .init(mainQueue: .main)

    var mainQueue: AnySchedulerOf<DispatchQueue>
}

// MARK: - Reducer

let gameReducer = GameReducer.combine([
    gameScriptReducer
        .pullback(
            state: \.gameScriptState,
            action: /GameAction.gameScriptAction,
            environment: {
                GameScriptEnvironment(mainQueue: $0.mainQueue)
            }
        ),
    gameSceneReducer
        .optional()
        .pullback(
            state: \.gameSceneState,
            action: /GameAction.gameSceneAction,
            environment: {
                GameSceneEnvironment(mainQueue: $0.mainQueue)
            }
        ),
    GameReducer { state, action, environment in
        switch action {

        case .toggleMap:
            state.isMapVisible = !state.isMapVisible
            return .none

        case .runSceneGame(let game):
            state.gameSceneState = .init(game: game)
            return .none

        // MARK: Game script actions
        case .gameScriptAction(_):
            return .none

        // MARK: Game scenes
        case .gameSceneAction(.finished):
            state.gameSceneState = nil
            return .init(value: .gameScriptAction(.nextStep))

        case .gameSceneAction(_):
            return .none
        }
    },
])
