import ComposableArchitecture
import SwiftUI

struct RequiredImageStepView: View {

    var image: GameRecognizableImage

    private var text: String {
        String(
            format: Resource.Text.AR.GameStep.requireImageInstruction.localized,
            image.title
        )
    }

    var body: some View {

        RequireImageFocusView(rect: .screenRect)
            .overlay(alignment: .bottomTrailing, content: content)
            .ignoresSafeArea()
    }

    private func content() -> some View {
        VStack(spacing: 15) {
            TextView(.verbatim(text), .stepsText)
                .foregroundColor(Theme.Colors.textColor)

            image.imageResource.image
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 150, height: 150)
                .cornerRadius(7)
        }
        .padding(.all, 20)
        .background(
            Resource.Image.message.image
                .resizable()
        )
    }
}

#if DEBUG

    struct RequiredImageStepView_Previews: PreviewProvider {
        static var previews: some View {
            RequiredImageStepView(
                image: .books
            )
            .previewInterfaceOrientation(.landscapeLeft)
        }
    }

#endif
