import RealityKit
import SwiftUI

struct RequireObjectStepView: View {

    struct ImageParameters {
        let resource: Resource.Image
        let size: CGSize
    }

    let text: String
    let object: GameRecognizableObject
    var image: ImageParameters? = nil

    var textObjectInstruction: String {
        String(
            format: Resource.Text.AR.GameStep.requireObjectInstruction
                .localized,
            object.title,
            object.title
        )
    }

    var body: some View {
        RequireImageFocusView(rect: .screenRect)
            .overlay(alignment: .bottomTrailing, content: content)
            .ignoresSafeArea()
    }

    private func content() -> some View {
        VStack(spacing: 20) {
            TextView(.verbatim(text), .stepsText)
                .foregroundColor(Theme.Colors.textColor)
                .frame(maxWidth: 250)

            VStack(spacing: 10) {
                TextView(.verbatim(textObjectInstruction), .stepsText)
                    .foregroundColor(Theme.Colors.textColor)
                imageView()
            }

        }
        .padding(.all, 20)
        .background(
            Resource.Image.message.image
                .resizable()
        )
    }

    @ViewBuilder
    func imageView() -> some View {
        if let imageParams = image {
            imageParams.resource.image
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(
                    width: imageParams.size.width,
                    height: imageParams.size.height
                )
                .cornerRadius(10)
        } else {
            EmptyView()
        }
    }

}

#if DEBUG
    struct RequireObjectStep_Previews: PreviewProvider {

        static let text =
            "A prosím tě, tady ještě děláme nový motiv naší pirátské vlajky, prosím mohl bys ho támhle na naší kopírce naskenovat a poslat mailem na recepcepraha@cngroup.dk. "
        static var previews: some View {
            Group {
                RequireObjectStepView(text: text, object: .printer)

                RequireObjectStepView(
                    text: text,
                    object: .printer,
                    image: .init(resource: .ar_bottle, size: .medium)
                )

            }
        }
    }

#endif
