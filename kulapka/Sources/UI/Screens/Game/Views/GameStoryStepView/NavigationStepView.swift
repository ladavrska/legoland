import ComposableArchitecture
import SwiftUI

struct NavigationStepView: View {

    let text: String
    let confirmBy: GameNavigationConfirmationOption
    let onTap: (() -> Void)?

    var body: some View {
        HStack {
            Spacer()

            VStack {
                Spacer()
                content()
            }
            .ignoresSafeArea()
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .background(focusImageBackground())
    }

    @ViewBuilder
    private func content() -> some View {
        VStack(spacing: 0) {
            TextView(.verbatim(text), .stepsText)
                .foregroundColor(Theme.Colors.textColor)
                .frame(maxWidth: 250)

            switch confirmBy {
            case .button(let buttonText):
                ButtonView(
                    text: buttonText,
                    style: .headingH3,
                    cornerRadius: 8,
                    innerPadding: .init(
                        top: 8,
                        leading: 15,
                        bottom: 8,
                        trailing: 15
                    )
                ) {
                    onTap?()
                }
                .frame(minWidth: 100)
                .padding()
                .foregroundColor(Color.white)

            case .image(let gameRecognizableImage):
                gameRecognizableImage.imageResource.image
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 150, height: 150)
                    .cornerRadius(7)
                    .padding()
            }
        }
        .padding(.all, 20)
        .background(
            Resource.Image.message.image
                .resizable()
        )
    }

    @ViewBuilder
    private func focusImageBackground() -> some View {
        switch confirmBy {
        case .button: EmptyView()
        case .image: RequireImageFocusView()
        }
    }
}

struct NavigationStepView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            NavigationStepView(
                text:
                    "Test test fdsfsd sdafsadfdsf fdfsadfasf asdfafsa fdsfasfsafsaf fdasfsd",
                confirmBy: .image(.bottle),
                onTap: {}
            )
            .previewInterfaceOrientation(.landscapeLeft)

            NavigationStepView(
                text: "Test test",
                confirmBy: .image(.bottle),
                onTap: {}
            )
            .previewInterfaceOrientation(.landscapeLeft)
        }
    }
}
