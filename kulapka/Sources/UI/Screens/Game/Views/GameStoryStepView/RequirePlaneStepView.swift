import SwiftUI

struct RequirePlaneStepView: View {

    let width: Float
    let height: Float

    var textProportions: String {
        String(
            format: Resource.Text.AR.GameStep.requirePlaneSpaceProportions
                .localized,
            "\(width) x \(height)"
        )
    }

    var body: some View {
        HStack {
            Spacer()
            VStack {
                TextView(
                    .verbatim(
                        Resource.Text.AR.GameStep.requirePlaneSpaceInstruction
                            .localized
                    ),
                    .stepsText
                )
                .foregroundColor(Theme.Colors.textColor)
                .padding(.all, 20)

                TextView(.verbatim(textProportions), .stepsText)
                    .foregroundColor(Theme.Colors.textColor)
                    .padding(.all, 20)
            }
            .background(Resource.Image.message.image.resizable())
            .frame(maxWidth: 250)
        }

    }
}
