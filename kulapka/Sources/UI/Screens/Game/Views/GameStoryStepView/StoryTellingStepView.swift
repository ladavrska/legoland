import Combine
import SwiftUI

struct StoryTellingStepView: View {

    let text: String
    let onEnd: () -> Void

    @State private var animationEnd = false
    @State private var animationDuration = 0
    @State private var textHeight: CGFloat = 0

    private let pointsAnimatedInOneSecond: CGFloat = 30
    private let timer = Timer.publish(every: 1, on: .main, in: .common)
        .autoconnect()
        .map { _ in () }
        .merge(with: Just(()))

    var body: some View {
        ZStack {

            Group {
                if animationEnd {
                    ScrollView {
                        storyTextView()
                    }
                    .padding(25)
                    .frame(width: 600)

                } else {
                    ZStack {
                        GeometryReader { proxy in
                            storyTextView()
                                .rotation3DEffect(
                                    .degrees(animationEnd ? 0 : 40),
                                    axis: (x: 1, y: 0, z: 0)
                                )
                                .shadow(
                                    color: .white.opacity(0.2),
                                    radius: 2,
                                    x: 0,
                                    y: 15
                                )
                                .fixedSize(horizontal: false, vertical: true)
                                .offset(
                                    y: animationEnd
                                        ? 0
                                        : countOffsetY(
                                            textHeight: proxy.frame(in: .local)
                                                .height
                                        )
                                )
                                .animation(
                                    .linear(duration: 1.0),
                                    value: animationDuration
                                )
                                .frame(width: 600)
                                .onAppear {
                                    self.textHeight =
                                        proxy.frame(in: .local).height
                                }
                        }
                        .frame(width: 600)

                        LinearGradient(
                            colors: [.clear, .black],
                            startPoint: .init(x: 0.5, y: 0.75),
                            endPoint: .init(x: 0.5, y: 0)
                        )
                    }
                    .onReceive(
                        timer,
                        perform: { interval in
                            guard textHeight > 0 else { return }
                            if pointsAnimatedInOneSecond
                                * CGFloat(animationDuration)
                                > (textHeight * 1.3)
                            {
                                withAnimation {
                                    animationEnd = true
                                }
                            } else {
                                withAnimation {
                                    self.animationDuration += 1
                                }
                            }
                        }
                    )
                }
            }
            controlButtonsLayer()
        }
    }

    @ViewBuilder
    func storyTextView() -> some View {
        TextView(.verbatim(text), .largeTitle)
            .foregroundColor(.white)
            .multilineTextAlignment(.center)
            .lineSpacing(20)
    }

    @ViewBuilder
    func controlButtonsLayer() -> some View {
        HStack {
            Spacer()
            VStack {
                Spacer()
                Button {
                    animationEnd
                        ? {
                            onEnd()
                        }()
                        : {
                            animationEnd = true
                        }()
                } label: {
                    Label {
                        TextView(
                            .verbatim(
                                animationEnd
                                    ? Resource.Text.AR.GameStep.storyDone
                                        .localized
                                    : Resource.Text.AR.GameStep.storySkip
                                        .localized
                            )
                        )
                        .foregroundColor(.white)
                    } icon: {
                        if animationEnd {
                            Image(systemName: "book.closed")
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .frame(width: 40, height: 40)
                                .foregroundColor(.white)
                        } else {
                            Resource.Image.pirate_back.image
                                .resizable()
                                .renderingMode(.template)
                                .aspectRatio(contentMode: .fit)
                                .frame(width: 40, height: 40)
                                .foregroundColor(.white)
                                .rotationEffect(.degrees(-90))
                        }
                    }
                }
            }
        }
        .padding(25)
    }

    private func countOffsetY(textHeight: CGFloat) -> CGFloat {
        let offset =
            textHeight
            - (pointsAnimatedInOneSecond * CGFloat(animationDuration))
        return offset - (pointsAnimatedInOneSecond * 2) - 120
    }
}

struct StoryTellingStep_Previews: PreviewProvider {

    static private let text: String =
        """
        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Praesent dapibus. Ut tempus purus at lorem. Fusce tellus odio, dapibus id fermentum quis, suscipit id erat. Pellentesque ipsum. Etiam egestas wisi a erat. Maecenas ipsum velit, consectetuer eu lobortis ut, dictum at dui. Duis condimentum augue id magna semper rutrum.

              Proin pede metus, vulputate nec, fermentum fringilla, vehicula vitae, justo. In sem justo, commodo ut, suscipit at, pharetra vitae, orci. Nullam sapien sem, ornare ac, nonummy non, lobortis a enim. Phasellus rhoncus. Pellentesque pretium lectus id turpis. Maecenas lorem. Curabitur ligula sapien, pulvinar a vestibulum quis, facilisis vel sapien. Mauris tincidunt sem sed arcu. Nunc tincidunt ante vitae massa. Maecenas sollicitudin. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Fusce consectetuer risus a nunc. Pellentesque pretium lectus id turpis. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat. Sed vel lectus. Donec odio tempus molestie, porttitor ut, iaculis quis, sem. Curabitur sagittis hendrerit ante. In enim a arcu imperdiet malesuada. Nullam lectus justo, vulputate eget mollis sed, tempor sed magna.
        """

    static var previews: some View {
        StoryTellingStepView(text: text, onEnd: {})
            .previewInterfaceOrientation(.landscapeRight)
    }
}
