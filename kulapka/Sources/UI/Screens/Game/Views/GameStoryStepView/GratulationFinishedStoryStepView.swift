import SwiftUI

struct GratulationFinishedStoryStepView: View {

    let parameters: GameGratulationFinishedStepParameters
    let onTap: () -> Void

    var body: some View {
        ZStack {
            Color.black.opacity(0.5)
            VStack(spacing: 10) {
                Image(systemName: "checkmark")
                    .resizable()
                    .scaledToFit()
                    .foregroundColor(.green)
                    .frame(width: 60, height: 60)

                TextView(.verbatim(parameters.gratulationText), .headingH2)
                    .foregroundColor(.white)

                VStack {
                    TextView(
                        .verbatim(parameters.nextInstructionText),
                        .stepsText
                    )
                    .foregroundColor(Theme.Colors.textColor)

                    ButtonView(
                        text: parameters.buttonText,
                        cornerRadius: 6,
                        innerPadding: .init(
                            top: 10,
                            leading: 10,
                            bottom: 10,
                            trailing: 10
                        ),
                        onTap: {
                            onTap()
                        }
                    )
                }
                .padding(20)
                .frame(maxWidth: 600)
                .background(
                    Resource.Image.message.image
                        .resizable()
                )

            }
        }
        .ignoresSafeArea()
    }
}

struct FinishStepView_Previews: PreviewProvider {
    static var previews: some View {
        GratulationFinishedStoryStepView(
            parameters: .init(
                gratulationText: "You're awesome!",
                nextInstructionText:
                    "Next task is to visit the big boss in the IT Department.",
                buttonText: "Let’s go!"
            ),
            onTap: {}
        )
        .previewInterfaceOrientation(.landscapeLeft)
    }
}
