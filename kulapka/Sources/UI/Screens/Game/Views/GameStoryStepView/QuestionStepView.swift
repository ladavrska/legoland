//
//  QuestionStepView.swift
//  kulapka
//
//  Created by Martin Matějka on 26.01.2022.
//

import SwiftUI

struct QuestionStepView: View {

    enum QuestionResult: Equatable {
        case right
        case wrong
    }

    struct Answer: Identifiable {
        var id: UUID { .init() }
        let text: String
        let isRight: Bool
        var isSelected: Bool
    }

    let question: String
    @State private var answers: [Answer]
    @State private var questionResult: QuestionResult?
    let onComplete: () -> Void

    var isMultipleAnswers: Bool {
        answers.filter { $0.isRight }.count > 1
    }

    var isDisabledCheckButton: Bool {
        guard isMultipleAnswers else { return false }
        return answers.filter { $0.isSelected }.count < 1
    }

    init(
        question: String,
        options: [QuestionOption],
        onComplete: @escaping () -> Void
    ) {
        self.question = question
        self.answers = options.map { option in
            switch option {
            case .right(let text):
                return .init(text: text, isRight: true, isSelected: false)
            case .wrong(let text):
                return .init(text: text, isRight: false, isSelected: false)
            }
        }
        self.onComplete = onComplete
    }

    var body: some View {
        HStack {
            Spacer()
            VStack {
                Spacer()
                content()
            }
        }
        .overlay(resultView())
        .onReceive(
            questionResult.publisher.compactMap { $0 }.delay(
                for: 3.0,
                scheduler: DispatchQueue.main
            ),
            perform: { output in
                questionResult = nil
                switch output {
                case .right:
                    onComplete()
                case .wrong: break
                }
            }
        )
    }

    @ViewBuilder
    private func resultView() -> some View {
        ZStack {
            if let result = questionResult {
                Color.black
                    .opacity(0.6)
                    .transition(.opacity)

                switch result {
                case .right:
                    VStack {
                        TextView(.verbatim("Great!"), .headingH2)
                            .foregroundColor(.green)
                        TextView(.verbatim("Yabba Dabba Doo..."), .headingH2)
                    }
                    .transition(.opacity)
                case .wrong:
                    VStack {
                        TextView(.verbatim("Ouch :{"), .headingH2)
                            .foregroundColor(.red)
                        TextView(.verbatim("Try again"), .headingH2)
                    }
                    .transition(.opacity)
                }
            } else {
                EmptyView()
            }
        }
        .animation(.default, value: questionResult)
    }

    @ViewBuilder
    private func content() -> some View {
        VStack {
            TextView(
                .verbatim(question),
                .stepsText
            )
            .foregroundColor(Theme.Colors.textColor)

            VStack(spacing: 10) {
                ForEach($answers) { $answer in
                    AnswerOptionView(
                        text: answer.text,
                        variant: isMultipleAnswers ? .multiple : .single,
                        isSelected: $answer.isSelected,
                        onTap: isMultipleAnswers
                            ? nil
                            : {
                                checkAnswers()
                            }
                    )
                }
            }

            if isMultipleAnswers {
                Button {
                    checkAnswers()
                } label: {
                    TextView(.verbatim("Check"))
                        .foregroundColor(.white)
                        .frame(height: 40)
                        .frame(maxWidth: .infinity)
                        .background(
                            QuestionButtonBackground(
                                color: Theme.Colors.textColor
                            )
                        )
                }
                .disabled(isDisabledCheckButton)
            }
        }
        .padding()
        .frame(maxWidth: 400)
        .background(Resource.Image.message.image.resizable())
    }

    private func checkAnswers() {
        let isRightAnswered: Bool =
            answers.filter { $0.isRight == $0.isSelected }.count
            == answers.count
        if isRightAnswered {
            questionResult = .right
            // onComplete
        } else {
            questionResult = .wrong
            resetAnswers()
        }
    }

    private func resetAnswers() {
        answers = answers.map {
            Answer(text: $0.text, isRight: $0.isRight, isSelected: false)
        }
    }
}

struct AnswerOptionView: View {

    enum Variant {
        case single, multiple
    }

    let text: String
    let variant: Variant
    @Binding var isSelected: Bool
    var onTap: (() -> Void)? = nil

    var body: some View {
        Button {
            isSelected.toggle()
            onTap?()
        } label: {
            HStack {
                TextView(.verbatim(text))
                    .foregroundColor(.black)
                if case .multiple = variant {
                    Spacer()
                    Toggle("IsSelected", isOn: $isSelected)
                        .labelsHidden()
                }
            }
            .padding(.horizontal, 15)
            .frame(height: 40)
            .frame(maxWidth: .infinity)
            .background(QuestionButtonBackground())
        }

    }
}

private struct QuestionButtonBackground: View {

    var color: Color = .white
    var body: some View {
        color
            .clipShape(
                RoundedRectangle(cornerSize: .init(width: 20, height: 20))
            )
            .shadow(color: .black, radius: 4, x: 0, y: 4)
    }
}

struct QuestionStepView_Previews: PreviewProvider {
    static var previews: some View {
        QuestionStepView(
            question:
                "You are in the social room, great! What do you see in front of you?",
            options: [
                .right("Coffee machine"),
                .wrong("Dishwasher"),
                .wrong("Big flower"),
            ],
            onComplete: {}
        )

        QuestionStepView(
            question:
                "You are in the social room, great! What do you see in front of you?",
            options: [
                .right("Coffee machine"),
                .right("Dishwasher"),
                .wrong("Big flower"),
            ],
            onComplete: {}
        )
    }
}
