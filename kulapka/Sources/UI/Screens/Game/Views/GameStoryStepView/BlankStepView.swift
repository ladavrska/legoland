import SwiftUI

struct BlankStepView: View {
    var body: some View {
        VStack(alignment: .center) {
            HStack(alignment: .center) {
                ZStack {
                    Circle().fill(Color.white).frame(width: 10, height: 10)
                }
            }
        }
    }
}
