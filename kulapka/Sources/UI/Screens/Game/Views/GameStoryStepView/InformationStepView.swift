import ComposableArchitecture
import SwiftUI

struct InformationStepView: View {

    struct ButtonProperties {
        let title: String
        let onTap: () -> Void
    }

    var title: String? = nil
    let text: String
    var button: ButtonProperties? = nil
    var image: GameInformationStepParameters.Image? = nil

    private var imageLayout: GameInformationStepParameters.Image.Layout {
        image?.layout ?? .top
    }

    var body: some View {

        HStack {
            Spacer()
            Group {
                switch imageLayout {
                case .leading:
                    HStack {
                        imageView()
                        VStack {
                            textView()
                            buttonView()
                        }
                    }
                case .trailing:
                    HStack {
                        VStack {
                            textView()
                            buttonView()
                        }
                        imageView()
                    }
                case .top:
                    VStack {
                        imageView()
                        textView()
                        buttonView()
                    }
                case .bottom:
                    VStack {
                        textView()
                        buttonView()
                        imageView()
                    }
                case .middle:
                    VStack {
                        textView()
                        imageView()
                        buttonView()
                    }
                }
            }
            .padding(20)
            .background(
                Resource.Image.message.image
                    .resizable()
            )
        }
    }

    @ViewBuilder
    func textView() -> some View {
        if let title = title {
            VStack {
                TextView(.verbatim(title), .headingH3)
                    .foregroundColor(Theme.Colors.textColor)
                    .frame(maxWidth: 300)
                TextView(.verbatim(text), .stepsText)
                    .foregroundColor(Theme.Colors.textColor)
                    .frame(maxWidth: 300)
            }
        } else {
            TextView(.verbatim(text), .stepsText)
                .foregroundColor(Theme.Colors.textColor)
                .frame(maxWidth: 300)
        }

    }

    @ViewBuilder
    func buttonView() -> some View {
        if let properties = button {
            ButtonView(
                text: properties.title,
                style: .headingH3,
                cornerRadius: 8,
                innerPadding: .init(
                    top: 8,
                    leading: 15,
                    bottom: 8,
                    trailing: 15
                )
            ) {
                properties.onTap()
            }
            .frame(minWidth: 100)
            .padding()
        } else {
            EmptyView()
        }
    }

    @ViewBuilder
    func imageView() -> some View {
        if let imageParams = image {
            imageParams.resource.image
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(
                    width: imageParams.size.width,
                    height: imageParams.size.height
                )
                .cornerRadius(10)
                .padding()
        } else {
            EmptyView()
        }
    }
}

struct ConfirmationStepView_Previews: PreviewProvider {
    static var previews: some View {
        InformationStepView(
            text: "Are you ready on the journey?",
            button: .init(title: "Yes, i am!", onTap: {}),
            image: .init(
                resource: .pirate,
                size: .init(width: 200, height: 200),
                layout: .leading
            )
        )
        .previewInterfaceOrientation(.landscapeLeft)
        //    .previewLayout(.sizeThatFits)

        InformationStepView(
            text: "Are you ready on the journey?",
            button: .init(title: "Yes, i am!", onTap: {}),
            image: .init(
                resource: .pirate,
                size: .init(width: 100, height: 100),
                layout: .trailing
            )
        )
        .previewLayout(.sizeThatFits)

        InformationStepView(
            text: "Are you ready on the journey?",
            button: .init(title: "Yes, i am!", onTap: {}),
            image: .init(
                resource: .pirate,
                size: .init(width: 300, height: 300),
                layout: .top
            )
        )
        .previewLayout(.sizeThatFits)

        InformationStepView(
            text: "Are you ready on the journey?",
            button: .init(title: "Yes, i am!", onTap: {}),
            image: .init(
                resource: .pirate,
                size: .init(width: 100, height: 100),
                layout: .middle
            )
        )
        .previewLayout(.sizeThatFits)

        InformationStepView(
            text: "Are you ready on the journey?",
            button: .init(title: "Yes, i am!", onTap: {}),
            image: .init(
                resource: .pirate,
                size: .init(width: 100, height: 100),
                layout: .bottom
            )
        )
        .previewLayout(.sizeThatFits)
    }
}
