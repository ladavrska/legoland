import ComposableArchitecture
import SwiftUI

struct ITRoomSceneView: View {

    typealias Text = Resource.Text.StoryText.ITRoom

    let store: ITRoomSceneStore
    let manager: MonkeySceneRenderManager

    var body: some View {
        WithViewStore(store) { viewStore in
            ZStack {
                switch viewStore.state.currentStep {
                case .welcomeToIT:
                    InformationStepView(
                        text: Resource.Text.StoryText.ITRoom.stepInstructions
                            .localized,
                        button: .init(
                            title: "OK",
                            onTap: {
                                viewStore.send(.setStep(.shootingMonkeys))
                            }
                        ),
                        image: .init(
                            resource: .pirate_skull_skeleton,
                            size: .big,
                            layout: .top
                        )
                    )
                    .transition(.move(edge: .trailing))
                case .shootingMonkeys:
                    HStack {
                        Spacer()
                        VStack {
                            TextView(
                                .verbatim(
                                    "Monkeys: \(viewStore.monkeysToShoot)"
                                ),
                                .headingH3
                            )
                            .padding()
                            Spacer()
                        }
                    }
                    .transition(.move(edge: .trailing))
                case .shootingCompleted:
                    InformationStepView(
                        text: Resource.Text.StoryText.ITRoom
                            .stepShootingCompleted.localized,
                        button: .init(
                            title: "OK",
                            onTap: {
                                viewStore.send(.finished)
                            }
                        ),
                        image: .init(
                            resource: .pirate_skull_skeleton,
                            size: .big,
                            layout: .top
                        )
                    )
                    .transition(.move(edge: .trailing))
                }
            }
            .animation(.default, value: viewStore.currentStep)
            .onReceive(manager.eventsPublisher) { event in
                switch event {
                case .finished:
                    viewStore.send(.setStep(.shootingCompleted))
                case .checkMonkeys: break
                }
            }
            .onReceive(manager.monkeysToShootPublisher) { count in
                viewStore.send(.setMonkeysToShoot(count))
            }
        }
    }
}

#if DEBUG
    struct ITRoomSceneView_Previews: PreviewProvider {
        static var previews: some View {
            ITRoomSceneView(store: .mockStore, manager: .mock)
        }
    }

#endif
