import ComposableArchitecture
import SwiftUI

// MARK: - Type-aliases

typealias ITRoomSceneStore = Store<ITRoomSceneState, ITRoomSceneAction>

extension ITRoomSceneStore {
    static var mockStore: Self {
        .init(
            initialState: .init(monkeys: .init()),
            reducer: itRoomSceneReducer,
            environment: .mock
        )
    }
}

typealias ITRoomSceneViewStore = ViewStore<
    ITRoomSceneState, ITRoomSceneAction
>
typealias ITRoomSceneReducer = Reducer<
    ITRoomSceneState, ITRoomSceneAction, ITRoomSceneEnvironment
>

// MARK: - State

enum ITRoomStep: Equatable {
    case welcomeToIT
    case shootingMonkeys
    case shootingCompleted
}

struct ITRoomSceneState: Equatable {
    let entity: Experience.Monkeys
    var currentStep: ITRoomStep = .welcomeToIT
    var monkeysToShoot: Int

    init(monkeys: Experience.Monkeys) {
        self.entity = monkeys
        self.monkeysToShoot = monkeys.monkeyEntities.count
    }
}

// MARK: - Actions

enum ITRoomSceneAction {
    case setStep(ITRoomStep)
    case setMonkeysToShoot(Int)
    case finished
}

// MARK: - Environment

struct ITRoomSceneEnvironment {
    static let mock: Self = .init(mainQueue: .main)

    var mainQueue: AnySchedulerOf<DispatchQueue>
}

// MARK: - Reducer

let itRoomSceneReducer = ITRoomSceneReducer { state, action, _ in
    switch action {
    case .setStep(let step):
        state.currentStep = step
        return .none
    case .setMonkeysToShoot(let count):
        state.monkeysToShoot = count
        return .none
    case .finished:
        return .none
    }
}
