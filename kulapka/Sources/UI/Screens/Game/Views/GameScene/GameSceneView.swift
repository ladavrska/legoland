import ComposableArchitecture
import SwiftUI

struct GameSceneView: View {

    let store: GameSceneStore

    var body: some View {
        WithViewStore(store) { viewStore in
            ZStack {
                IfLetStore.init(
                    store.scope(
                        state: { $0.socialRoomGameState },
                        action: GameSceneAction.socialRoomAction
                    )
                ) { store in
                    SocialRoomSceneView(store: store)
                }

                IfLetStore.init(
                    store.scope(
                        state: { $0.warehouseSceneState },
                        action: GameSceneAction.warehouseAction
                    )
                ) { store in
                    if case .warehouse(let params) = viewStore.game {
                        WarehouseSceneView(
                            store: store,
                            manager: params.manager
                        )
                    }
                }

                IfLetStore.init(
                    store.scope(
                        state: { $0.itRoomSceneState },
                        action: GameSceneAction.itRoomAction
                    )
                ) { store in
                    if case .itRoom(let params) = viewStore.game {
                        ITRoomSceneView(store: store, manager: params.manager)
                    }
                }
            }
        }
    }
}

struct GameSceneView_Previews: PreviewProvider {
    static var previews: some View {
        GameSceneView(store: .mockStore)
    }
}
