import ComposableArchitecture
import RealityKit
import SwiftUI

struct SocialRoomSceneView: View {

    typealias Text = Resource.Text.StoryText.SocialRoom

    let store: SocialRoomStore

    var body: some View {
        WithViewStore(store) { viewStore in
            ZStack {
                switch viewStore.currentStep {
                case .requireEmptyDrawer:
                    EmptyView()
//                    InformationStepView(
//                        title: Text.stepDragdrawerFullTitle.localized,
//                        text: Text.stepDragdrawerFullText.localized
//                    )
//                    .transition(.move(edge: .trailing))
                case .requireCup:
                    InformationStepView(
                        title: Text.stepRequireCupTitle.localized,
                        text: Text.stepRequireCupText.localized
                    )
                    .transition(.move(edge: .trailing))
                case .requireCoffeeButtonPress:
                    InformationStepView(
                        text: Text.stepPressCoffeeButtonText.localized
                    )
                    .transition(.move(edge: .trailing))

                case .doingCoffee:
                    InformationStepView(
                        text: Text.coffeePreparationDescription.localized
                    )
                    .transition(.move(edge: .trailing))
                case .doneCoffee:
                    InformationStepView(
                        text: Text.coffeeDoneInstruction.localized
                    )
                    .transition(.move(edge: .trailing))
                case .requireOrange:
                    InformationStepView(
                        title: Text.stepRequireOrangeTitle.localized,
                        text: Text.stepRequireOrangeText.localized,
                        button: nil,
                        image: .init(
                            resource: .pirate_bartender,
                            size: .medium,
                            layout: .top
                        )
                    )
                    .transition(.move(edge: .trailing))
                case .shakingDrink(let stage):
                    switch stage {
                    case .checkFridge:
                        InformationStepView(
                            title: Text.stepCheckFridgeTitle.localized,
                            text: Text.stepCheckFridgeText.localized,
                            image: .init(
                                resource: .pirate_bartender,
                                size: .medium,
                                layout: .bottom
                            )
                        )
                        .transition(.move(edge: .trailing))
                    case .playPlaystation:
                        InformationStepView(
                            text: Text.stepPlayPlaystationText.localized,
                            image: .init(
                                resource: .pirate_bartender,
                                size: .medium,
                                layout: .trailing
                            )
                        )
                        .transition(.move(edge: .trailing))
                    case .finished:
                        InformationStepView(
                            text: Text.lastInstructionsText.localized,
                            button: .init(
                                title: "OK",
                                onTap: {
                                    viewStore.send(.finished)
                                }
                            ),
                            image: .init(
                                resource: .pirate_bartender,
                                size: .medium,
                                layout: .leading
                            )
                        )
                        .transition(.move(edge: .trailing))
                    }
                }
            }
            .animation(.default, value: viewStore.currentStep)
            .onAppear {
                DispatchQueue.main.asyncAfter(
                    deadline: .now() + .milliseconds(500)
                ) {
                    viewStore.state.entity.notifications.requireEmptyDrawer
                        .post()
                }
                viewStore.entity.actions.emptyDrawer.onAction = { _ in
                    viewStore.state.entity.notifications.requireCup.post()
                    viewStore.send(.setStep(.requireCup))
                }
                viewStore.state.entity.actions.pressedCoffeeButton.onAction = {
                    _ in
                    viewStore.send(.setStep(.doingCoffee))
                }
                viewStore.state.entity.actions.gotCup.onAction = { _ in
                    viewStore.state.entity.notifications
                        .requireCoffeeButtonPress.post()
                    viewStore.send(.setStep(.requireCoffeeButtonPress))
                }
                viewStore.state.entity.actions.endPreparingCoffee.onAction = {
                    _ in
                    viewStore.send(.setStep(.doneCoffee))
                }
                viewStore.state.entity.actions.tappedDoneCoffee.onAction = {
                    _ in
                    viewStore.state.entity.notifications.requireOrange.post()
                    viewStore.send(.setStep(.requireOrange))
                }
                viewStore.entity.actions.tappedOrange.onAction = { _ in
                    viewStore.state.entity.notifications.requireDrink.post()
                    viewStore.send(.setStep(.shakingDrink(.checkFridge)))
                    DispatchQueue.main.asyncAfter(
                        deadline: .now() + .seconds(7)
                    ) {
                        viewStore.send(
                            .setStep(.shakingDrink(.playPlaystation))
                        )
                    }
                }
                viewStore.entity.actions.finishedDrink.onAction = { _ in
                    viewStore.send(.setStep(.shakingDrink(.finished)))
                }
            }
        }
    }
}

#if DEBUG
    struct SocialRoomSceneView_Previews: PreviewProvider {
        static var previews: some View {
            SocialRoomSceneView(store: .mockStore)
        }
    }
#endif
