import Combine
import ComposableArchitecture
import SwiftUI

// MARK: - Type-aliases

typealias SocialRoomStore = Store<SocialRoomState, SocialRoomAction>

extension SocialRoomStore {
    static var mockStore: Self {
        .init(
            initialState: .init(entity: .init()),
            reducer: socialRoomReducer,
            environment: .mock
        )
    }
}

typealias SocialRoomViewStore = ViewStore<SocialRoomState, SocialRoomAction>
typealias SocialRoomReducer = Reducer<
    SocialRoomState, SocialRoomAction, SocialRoomEnvironment
>

// MARK: - State

enum SocialRoomStep: Equatable {
    case requireEmptyDrawer
    case requireCup
    case requireCoffeeButtonPress
    case doingCoffee
    case doneCoffee
    case requireOrange
    case shakingDrink(ShakingDrinkStage)

    enum ShakingDrinkStage: Equatable {
        case checkFridge
        case playPlaystation
        case finished
    }
}

struct SocialRoomState: Equatable {

    let entity: Experience.SocialRoom
    var currentStep: SocialRoomStep = .requireEmptyDrawer
}

// MARK: - Actions

enum SocialRoomAction {
    case setStep(SocialRoomStep)
    case finished
}

// MARK: - Environment

struct SocialRoomEnvironment {
    static let mock: Self = .init(mainQueue: .main)

    var mainQueue: AnySchedulerOf<DispatchQueue>
}

// MARK: - Reducer

let socialRoomReducer = SocialRoomReducer { state, action, _ in
    switch action {
    case .setStep(let step):
        state.currentStep = step
        return .none
    case .finished:
        return .none
    }
}
