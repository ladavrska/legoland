import ComposableArchitecture
import SwiftUI

struct WarehouseSceneView: View {

    typealias Text = Resource.Text.StoryText.Warehouse

    let store: WarehouseSceneStore
    let manager: WarehouseSceneRenderManager

    var body: some View {
        WithViewStore(store) { viewStore in
            ZStack {

                switch viewStore.state.currentStep {
                case .welcomeGirl:
                    InformationStepView(
                        text:
                            "Hej, plavčíku! Ahoy! To jsem ráda, že jsi tu. Zrovna se tu stavil pošťák a přivezl zásoby. Pomůžeš mi s nimi prosím?",
                        button: .init(
                            title: "OK",
                            onTap: {
                                viewStore.send(.setStep(.placingBoxes))
                            }
                        ),
                        image: .init(
                            resource: .girl_warehouse,
                            size: .big,
                            layout: .top
                        )
                    )
                    .transition(.move(edge: .trailing))
                case .placingBoxes:
                    HStack {
                        Spacer()
                        VStack {
                            TextView(
                                .verbatim("Boxes: \(viewStore.boxesToCollect)"),
                                .headingH3
                            )
                            .padding()
                            Spacer()
                        }
                    }
                    .transition(.move(edge: .trailing))
                case .instructionsToTakeAKey:
                    InformationStepView(
                        text:
                            "Díky moc za pomoc! Ty jsi ten nový plavčík, že? Tady máš klíč od svojí kajuty. (Take a key)",
                        button: nil,
                        image: .init(
                            resource: .girl_warehouse,
                            size: .medium,
                            layout: .leading
                        )
                    )
                    .transition(.move(edge: .trailing))
                case .instructionsAboutKeysFromMeetingRooms:
                    InformationStepView(
                        text:
                            "Máme klíče od všeho. Kdybys někdy nějaké potřeboval, zastav se, zařídíme. Například támhle vedle dveří v krabičce visí klíče o zasedaček.",
                        button: .init(
                            title: "OK",
                            onTap: {
                                viewStore.send(.finished)
                            }
                        ),
                        image: .init(
                            resource: .girl_warehouse,
                            size: .medium,
                            layout: .trailing
                        )
                    )
                    .transition(.move(edge: .trailing))
                }
            }
            .animation(.default, value: viewStore.currentStep)
            .onAppear {
                viewStore.state.entity.actions.tappedKey.onAction = { _ in
                    viewStore.send(
                        .setStep(.instructionsAboutKeysFromMeetingRooms)
                    )
                }
            }
            .onReceive(manager.eventsPublisher) { event in
                switch event {
                case .finished:
                    viewStore.send(.setStep(.instructionsToTakeAKey))
                    viewStore.state.entity.notifications.finishedPlacingBoxes
                        .post()
                case .checkBoxes: break
                }
            }
            .onReceive(manager.boxesToCollectPublisher) { count in
                viewStore.send(.setBoxesToCollect(count))
            }
        }
    }
}

#if DEBUG
    struct WarehouseSceneView_Previews: PreviewProvider {
        static var previews: some View {
            WarehouseSceneView(store: .mockStore, manager: .mock)
        }
    }

#endif
