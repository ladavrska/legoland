import ComposableArchitecture
import SwiftUI

// MARK: - Type-aliases

typealias WarehouseSceneStore = Store<WarehouseSceneState, WarehouseSceneAction>

extension WarehouseSceneStore {
    static var mockStore: Self {
        .init(
            initialState: .init(warehouse: .init()),
            reducer: warehouseSceneReducer,
            environment: .mock
        )
    }
}

typealias WarehouseSceneViewStore = ViewStore<
    WarehouseSceneState, WarehouseSceneAction
>
typealias WarehouseSceneReducer = Reducer<
    WarehouseSceneState, WarehouseSceneAction, WarehouseSceneEnvironment
>

// MARK: - State

enum WarehouseStep: Equatable {
    case welcomeGirl
    case placingBoxes
    case instructionsToTakeAKey
    case instructionsAboutKeysFromMeetingRooms
}

struct WarehouseSceneState: Equatable {
    let entity: Experience.Warehouse
    var currentStep: WarehouseStep = .welcomeGirl
    var boxesToCollect: Int

    init(warehouse: Experience.Warehouse) {
        self.entity = warehouse
        self.boxesToCollect = warehouse.boxEntities.count
    }
}

// MARK: - Actions

enum WarehouseSceneAction {
    case setStep(WarehouseStep)
    case setBoxesToCollect(Int)
    case finished
}

// MARK: - Environment

struct WarehouseSceneEnvironment {
    static let mock: Self = .init(mainQueue: .main)

    var mainQueue: AnySchedulerOf<DispatchQueue>
}

// MARK: - Reducer

let warehouseSceneReducer = WarehouseSceneReducer { state, action, _ in
    switch action {
    case .setStep(let step):
        state.currentStep = step
        return .none
    case .setBoxesToCollect(let count):
        state.boxesToCollect = count
        return .none
    case .finished:
        return .none
    }
}
