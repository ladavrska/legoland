import ComposableArchitecture
import SwiftUI

// MARK: - Type-aliases

typealias GameSceneStore = Store<GameSceneState, GameSceneAction>

extension GameSceneStore {
    static var mockStore: Self {
        .init(
            initialState: .init(game: .socialRoom(.init(entity: .init()))),
            reducer: gameSceneReducer,
            environment: .mock
        )
    }
}

typealias GameSceneViewStore = ViewStore<GameSceneState, GameSceneAction>
typealias GameSceneReducer = Reducer<
    GameSceneState, GameSceneAction, GameSceneEnvironment
>

// MARK: - State

struct GameSceneState: Equatable {

    var socialRoomGameState: SocialRoomState?
    var warehouseSceneState: WarehouseSceneState?
    var itRoomSceneState: ITRoomSceneState?

    var game: ExperienceSceneGame

    init(game: ExperienceSceneGame) {
        self.game = game
        switch game {
        case .socialRoom(let params):
            socialRoomGameState = .init(entity: params.entity)
        case .warehouse(let params):
            warehouseSceneState = .init(warehouse: params.entity)
        case .itRoom(let params):
            itRoomSceneState = .init(monkeys: params.entity)
//        case .legoFigure(let params):
//            socialRoomGameState = .init(entity: params.entity)
        }
    }
}

// MARK: - Actions

enum GameSceneAction {
    case socialRoomAction(SocialRoomAction)
    case warehouseAction(WarehouseSceneAction)
    case itRoomAction(ITRoomSceneAction)
    case finished
}

// MARK: - Environment

struct GameSceneEnvironment {
    static let mock: Self = .init(mainQueue: .main)

    var mainQueue: AnySchedulerOf<DispatchQueue>
}

// MARK: - Reducer

let gameSceneReducer = GameSceneReducer.combine([
    socialRoomReducer
        .optional()
        .pullback(
            state: \.socialRoomGameState,
            action: /GameSceneAction.socialRoomAction,
            environment: {
                SocialRoomEnvironment(mainQueue: $0.mainQueue)
            }
        ),
    warehouseSceneReducer
        .optional()
        .pullback(
            state: \.warehouseSceneState,
            action: /GameSceneAction.warehouseAction,
            environment: {
                WarehouseSceneEnvironment(mainQueue: $0.mainQueue)
            }
        ),
    itRoomSceneReducer
        .optional()
        .pullback(
            state: \.itRoomSceneState,
            action: /GameSceneAction.itRoomAction,
            environment: {
                ITRoomSceneEnvironment(mainQueue: $0.mainQueue)
            }
        ),
    GameSceneReducer { state, action, _ in
        switch action {

        // MARK: - Warehouse actions

        case .warehouseAction(.finished):
            return .init(value: .finished)

        case .warehouseAction(_):
            return .none

        // MARK: - Social room actions
        case .socialRoomAction(.finished):
            return .init(value: .finished)

        case .socialRoomAction(_):
            return .none

        case .finished:
            return .none
        case .itRoomAction(.finished):
            return .init(value: .finished)
        case .itRoomAction(_):
            return .none
        }
    },
])
