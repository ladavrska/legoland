import Combine
import ComposableArchitecture

typealias GameScriptStore = Store<
    GameScriptState, GameScriptAction
>

public struct GameScriptState: Equatable {
    enum GameProgressState: Equatable {
        case ready
        case running(_ currentStoryIndex: Int, _ currentStepIndex: Int)
        case end
    }

    let script: GameScript
    var progressState: GameProgressState = .ready

    var currentStep: GameStoryStep? {
        guard
            case let .running(currentStoryIndex, currentStepIndex) =
                progressState
        else { return nil }
        return HardcodedGameScriptProvider.getStep(
            in: script,
            storyIndex: currentStoryIndex,
            stepIndex: currentStepIndex
        )
    }

    var currentStory: GameStory? {
        guard case let .running(currentStoryIndex, _) = progressState else {
            return nil
        }
        return GameStory(rawValue: currentStoryIndex)
    }

    var currentLocation: GameMapImage = .none

    init(setting: Setting) {
        let scriptProvider = HardcodedGameScriptProvider(setting: setting)
        self.script = scriptProvider.script
    }
}

enum GameScriptAction: Equatable {
    case startGame
    case nextStep
}

struct GameScriptEnvironment {
    var mainQueue: AnySchedulerOf<DispatchQueue>
    var getStep:
        (_ script: GameScript, _ storyIndex: Int, _ stepIndex: Int) ->
            GameStoryStep?

    init(mainQueue: AnySchedulerOf<DispatchQueue>) {
        self.mainQueue = mainQueue
        self.getStep = { script, storyIndex, stepIndex in
            return HardcodedGameScriptProvider.getStep(
                in: script,
                storyIndex: storyIndex,
                stepIndex: stepIndex
            )
        }
    }
}

// MARK: - Reducer

let gameScriptReducer = Reducer<
    GameScriptState, GameScriptAction,
    GameScriptEnvironment
> { state, action, environment in
    switch action {
    case .startGame:
        state.progressState = .running(0, 0)

        // Starting map location
        if case let .navigation(params) = state.currentStep {
            state.currentLocation = params.map
        }

        return .none
    case .nextStep:
        guard
            case let .running(currentStoryIndex, currentStepIndex) = state
                .progressState
        else {
            return .none
        }
        let nextStepIndex = currentStepIndex + 1
        if environment.getStep(state.script, currentStoryIndex, nextStepIndex)
            != nil
        {
            // Next step in story
            state.progressState = .running(currentStoryIndex, nextStepIndex)
        } else if GameStory(rawValue: currentStoryIndex + 1) != nil {
            // New story
            state.progressState = .running(currentStoryIndex + 1, 0)
        } else {
            // END of a game
            state.progressState = .end
        }

        // update map location if required
        if case let .navigation(params) = state.currentStep {
            state.currentLocation = params.map
        }
        return .none
    }
}
