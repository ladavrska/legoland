import ComposableArchitecture
import SwiftUI

struct SettingScreenView: View {

    @Environment(\.colorScheme) var colorScheme

    let store: SettingScreenStore

    @State private var isVisibleLanguagePopover: Bool = false
    @State private var isVisiblePersonPicker: Bool = false
    @State private var isVisibleLandingDestinationPicker: Bool = false

    var backgroundColor: Color {
        colorScheme == .dark ? .black : .white
    }

    var body: some View {
        WithViewStore(store) { viewStore in
            Form {
                HStack {
                    TextView(
                        .verbatim(Resource.Text.Setting.language.localized)
                    )
                    Spacer()

                    PickerView<LocaleCode>.init(
                        selectedOption: viewStore.localCode,
                        isVisible: $isVisibleLanguagePopover,
                        onSelect: { localCode in
                            viewStore.send(.setLocalCode(code: localCode))
                        }
                    )
                }

                Section(Resource.Text.Setting.shipSetting.localized) {
                    HStack {
                        TextView(
                            .verbatim(Resource.Text.Setting.crew.localized)
                        )
                        Spacer()

                        PickerView<PersonSetting>.init(
                            selectedOption: viewStore.personSetting,
                            isVisible: $isVisiblePersonPicker
                        ) { setting in
                            viewStore.send(.setPersonSetting(setting))
                        }
                    }

                    TextField(
                        viewStore.personSetting.placeholder,
                        text: viewStore.binding(
                            get: { $0.personName },
                            send: SettingScreenAction.onEditPersonName
                        )
                    )
                    .padding(.vertical, 10)

                    HStack {
                        TextView(
                            .verbatim(
                                Resource.Text.Setting.landingDestination
                                    .localized
                            )
                        )
                        Spacer()

                        PickerView<LandingDestination>.init(
                            selectedOption: viewStore.landingDestination,
                            isVisible: $isVisibleLandingDestinationPicker
                        ) { landingDestination in
                            viewStore.send(
                                .setLandingDestination(landingDestination)
                            )
                        }
                    }

                }
            }
        }
        .navigationBarTitle(
            Resource.Text.Setting.title.localized,
            displayMode: .large
        )
    }
}

extension LocaleCode: PickerViewOption {
    var title: String {
        switch self {
        case .cs: return "🇨🇿 \(Resource.Text.Setting.languageCs.localized)"
        case .en: return "🇬🇧 \(Resource.Text.Setting.languageEn.localized)"
        }
    }

    var image: Image? {
        return nil
    }
}

extension PersonSetting: PickerViewOption {

    var title: String {
        switch self {
        case .single: return Resource.Text.Setting.personSingle.localized
        case .group: return Resource.Text.Setting.personGroup.localized
        }
    }

    var image: Image? {
        switch self {
        case .single: return Image(systemName: "person.fill")
        case .group: return Image(systemName: "person.3.fill")
        }
    }
}

extension PersonSetting {

    fileprivate var placeholder: String {
        switch self {
        case .single:
            return Resource.Text.Setting.personSingleNamePlaceholder.localized
        case .group:
            return Resource.Text.Setting.personGroupNamePlaceholder.localized
        }
    }
}

extension LandingDestination: PickerViewOption {
    var title: String {
        switch self {
        case .prague: return Resource.Text.LandingDestination.prague.localized
        case .bratislav:
            return Resource.Text.LandingDestination.bratislav.localized
        case .zlin: return Resource.Text.LandingDestination.zlin.localized
        case .bucharest:
            return Resource.Text.LandingDestination.bucharest.localized
        case .hradecKralove:
            return Resource.Text.LandingDestination.hradecKralove.localized
        }
    }

    var image: Image? {
        return Image(systemName: "building.2")
    }
}

#if DEBUG
    struct SettingScreenView_Previews: PreviewProvider {
        static var previews: some View {
            SettingScreenView(store: .mockStore)
        }
    }
#endif
