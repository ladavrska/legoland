import ComposableArchitecture
import SwiftUI

// MARK: - Type-aliases

typealias SettingScreenStore = Store<SettingScreenState, SettingScreenAction>

extension SettingScreenStore {
    static var mockStore: Self {
        .init(
            initialState: .init(
                localCode: .en,
                personSetting: .single,
                landingDestination: .prague,
                personName: .init("Jack Sparrow")
            ),
            reducer: settingScreenReducer,
            environment: .mock
        )
    }
}

typealias SettingScreenViewStore = ViewStore<
    SettingScreenState, SettingScreenAction
>
typealias SettingScreenReducer = Reducer<
    SettingScreenState, SettingScreenAction, SettingScreenEnvironment
>

// MARK: - State

struct SettingScreenState: Equatable {
    var localCode: LocaleCode
    var personSetting: PersonSetting
    var landingDestination: LandingDestination
    var personName: String
}

// MARK: - Actions

enum SettingScreenAction {
    case setLocalCode(code: LocaleCode)
    case setPersonSetting(PersonSetting)
    case setLandingDestination(LandingDestination)
    case onEditPersonName(String)
}

// MARK: - Environment

struct SettingScreenEnvironment {
    static let mock: Self = .init(mainQueue: .main)

    var mainQueue: AnySchedulerOf<DispatchQueue>
}

// MARK: - Reducer

let settingScreenReducer = SettingScreenReducer { state, action, _ in
    switch action {
    case .setLocalCode(let code):
        Localization.localeCode = code
        state.localCode = code
        return .none
    case .setPersonSetting(let setting):
        state.personSetting = setting
        return .none
    case .setLandingDestination(let destination):
        state.landingDestination = destination
        return .none
    case .onEditPersonName(let name):
        state.personName = name
        return .none
    }
}
