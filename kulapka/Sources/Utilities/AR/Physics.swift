import Foundation
import RealityKit

struct Physics {
    enum Body {
        case Dynamic
        case Kinematic

        var component: PhysicsBodyComponent {
            switch self {
            case .Dynamic:
                return PhysicsBodyComponent(
                    massProperties: .default,
                    material: .generate(friction: 1000, restitution: 0),
                    mode: .dynamic
                )
            case .Kinematic:
                return PhysicsBodyComponent(
                    massProperties: .default,
                    material: .generate(friction: 1000, restitution: 0),
                    mode: .kinematic
                )
            }
        }
    }
}
