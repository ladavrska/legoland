import ARKit
import Combine
import Foundation
import RealityKit

class EntityFactory {

    enum FactoryError: LocalizedError {
        case noModel
    }

    static func createEntity(
        for object: HasRenderableObjectItem
    ) -> AnyPublisher<
        Result<Entity, Error>, Never
    > {
        return createEntity(for: object.renderableItem)
    }

    static func createEntity(
        for item: RenderableObjectItem
    ) -> AnyPublisher<
        Result<Entity, Error>, Never
    > {
        switch item {
        case .foundImageScene:
            return Future<Result<Entity, Error>, Never> { promise in
                Experience.loadImageFoundAsync { result in
                    promise(
                        .success(
                            result.map({ imageFound in
                                return imageFound as Entity
                            })
                        )
                    )
                }
            }.eraseToAnyPublisher()
        case .printerScene:
            return Future<Result<Entity, Error>, Never> { promise in
                Experience.loadPrinterAsync { result in
                    promise(
                        .success(
                            result.map({ printer in
                                return printer as Entity
                            })
                        )
                    )
                }
            }.eraseToAnyPublisher()
        case .warehouseScene:
            return Future<Result<Entity, Error>, Never> { promise in
                do {
                    let warehouse = try Experience.loadWarehouse()
                    promise(.success(.success(warehouse)))
                } catch {
                    promise(.success(.failure(error)))
                }
            }.eraseToAnyPublisher()
        case .earthObject:
            return Just(())
                .map { _ in
                    return getModel(for: item)
                }
                .tryMap { maybeModel -> AnyPublisher<Entity, Error> in
                    guard let model = maybeModel else {
                        throw FactoryError.noModel
                    }
                    return
                        try model
                        .loadAsync()
                        .eraseToAnyPublisher()
                }
                .flatMap { $0 }
                .catchToResult()
                .eraseToAnyPublisher()
        case .bookObject:
            return Just(())
                .map { _ in
                    return getModel(for: item)
                }
                .tryMap { maybeModel -> AnyPublisher<Entity, Error> in
                    guard let model = maybeModel else {
                        throw FactoryError.noModel
                    }
                    return
                        try model
                        .loadAsync()
                        .eraseToAnyPublisher()
                }
                .flatMap { $0 }
                .catchToResult()
                .eraseToAnyPublisher()
        case .socialRoomScene:
            return Future<Result<Entity, Error>, Never> { promise in
                Experience.loadSocialRoomAsync { result in
                    promise(
                        .success(
                            result.map { socialRoom in socialRoom as Entity }
                        )
                    )
                }
            }.eraseToAnyPublisher()
        case .ITRoomScene:
            return Future<Result<Entity, Error>, Never> { promise in
                do {
                    let monkeys = try Experience.loadMonkeys()
                    promise(.success(.success(monkeys)))
                } catch {
                    promise(.success(.failure(error)))
                }
            }.eraseToAnyPublisher()
        }
    }

    static func createEntity(for item: RenderableObject) -> AnyPublisher<
        Result<Entity, Error>, Never
    > {
        return Just(.failure(FactoryError.noModel)).eraseToAnyPublisher()
    }

    private static func getModel(for item: RenderableObjectItem) -> Resource
        .USDZFile?
    {
        switch item {
        case .earthObject:
            return .earth
        case .bookObject:
            return .books
        case .printerScene: return nil
        case .foundImageScene: return nil
        case .warehouseScene: return nil
        case .socialRoomScene: return nil
        case .ITRoomScene: return nil
        }
    }
}
