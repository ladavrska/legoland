import Foundation

class ScriptTextProvider {

    static let level1Story1: String = """
          Legenda praví, že před třiceti lety vypluli vikingští piráti z Dánska dobývat střední Evropu, a narazili na ostrov Praha, který skrýval velký potenciál. Původně se rozhodli zakotvit v zátoce Dejvice, nicméně bylo tam málo děvek. Rozhodli se tedy přesunout po řece hlouběji do centra ostrova. Kotviště Nové Město vypadalo v tomto ohledu mnohem lépe.

          Yada yada.

          Plavčík / skupina plavčíků se vydala na průzkum ostrova.
        """

    static let level1Story2: String = """
        Maskot: "Vítej Plavčíku!

        Jistě jsi přišel hledat poklad. Mám pro tebe dobrou zprávu - zprávu v lahvi. Někde by tady měla být."

        [přepne se na kameru a očekává se, že hráč naskenuje obrázek lahve se zprávou, který je v recepci]

        //Poznámka: buď mu pomůže onboarderka nebo to může být v aplikaci jako nápověda.

        Maskot předá lahev, v té je srolovaná mapa. Mapa ukazuje čárkovanou cestu do patra do social roomu.

        Maskot: "Nejprve by ses měl posilnit na cestu. Vydej se tedy do baru Hula Hula a hledej drink s paraplíčkem. Přeji dobrý vítr! Yarrr!"

        Aplikace se přepne do map modu, ukazuje cestu ke schodům a dotaz "Už jsi v prvním patře?", pokud ano, mapa se posune, ukazuje cestu od schodů do social roomu, kde je malý křížek. A dotaz: "Už jsi na místě?" Pokud ano, přepne se do kamera modu. Když hráč načte obrázek drinku s paraplíčkem, naskočí další scéna.
        """

}
