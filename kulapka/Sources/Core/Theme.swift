import SwiftUI

struct Theme {

    struct Fonts {
        static func mainFont(_ size: CGFloat) -> Font {
            return .custom("Rakkas-Regular", size: size)
        }
    }

    struct Colors {
        static let background: Color = .red
        static let dividerColor = Color.black
        static let textColor = Resource.Color.selected.color
        static let imageColor = Resource.Color.selected.color
        static let buttonColor = Resource.Color.primary.color
    }
}
