import Foundation

struct AppConstant {

    #if DEBUG
        static let isDebug = true
    #else
        static let isDebug = false
    #endif

}
