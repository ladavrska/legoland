import Combine
import Foundation

public enum LocaleCode: String, Equatable, CaseIterable, Identifiable {
    case en
    case cs

    public static let `default`: Self = .en

    public var id: String {
        self.rawValue
    }
}

public struct Localization: Equatable {
    static public var localeCode = LocaleCode.default

    static public func setLocalCode(_ localeCode: LocaleCode) {
        self.localeCode = localeCode
    }
}

extension String {
    public var localized: String {
        let language = Localization.localeCode.rawValue
        let path = Bundle.main.path(forResource: language, ofType: "lproj")
        let bundle = Bundle(path: path!)!

        let localized = bundle.localizedString(
            forKey: self,
            value: nil,
            table: nil
        )
        if localized == self {
            print(
                "[Localization] Missing localization: \"\(self)\"=\"\(localized)\";"
            )
        }
        return localized
    }
}
