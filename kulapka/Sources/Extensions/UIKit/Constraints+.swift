import UIKit

extension UIView {
    func constraintToParent(
        withInsets insets: UIEdgeInsets = UIEdgeInsets.zero
    ) {
        guard let superview = superview else {
            return
        }
        translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            leftAnchor.constraint(
                equalTo: superview.leftAnchor,
                constant: insets.left
            ),
            rightAnchor.constraint(
                equalTo: superview.rightAnchor,
                constant: -insets.right
            ),
            topAnchor.constraint(
                equalTo: superview.topAnchor,
                constant: insets.top
            ),
            bottomAnchor.constraint(
                equalTo: superview.bottomAnchor,
                constant: -insets.bottom
            ),
        ])
    }
}
