import ARKit
import Foundation

extension ARViewController {

    func createCoachingOverlay(
        goal: ARCoachingOverlayView.Goal,
        delegate: ARCoachingOverlayViewDelegate,
        session: ARSession,
        automatically: Bool
    ) -> ARCoachingOverlayView {
        return {
            let coachingOverlay = ARCoachingOverlayView()
            coachingOverlay.goal = goal
            // 1. Link The GuidanceOverlay To Our Current Session
            coachingOverlay.session = session
            coachingOverlay.delegate = delegate
            // 3. Enable The Overlay To Activate Automatically Based On User Preference
            coachingOverlay.activatesAutomatically = automatically
            return coachingOverlay
        }()
    }

    func checkARSupport() {
        if !ARWorldTrackingConfiguration.isSupported {
            fatalError(
                """
                    ARKit is not available on this device. For apps that require ARKit
                    for core functionality, use the `arkit` key in the key in the
                    `UIRequiredDeviceCapabilities` section of the Info.plist to prevent
                    the app from installing. (If the app can't be installed, this error
                    can't be triggered in a production scenario.)
                    In apps where AR is an additive feature, use `isSupported` to
                    determine whether to show UI for launching AR experiences.
                """
            )
        }
    }
}
