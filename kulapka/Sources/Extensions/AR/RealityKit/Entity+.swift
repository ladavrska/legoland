import Foundation
import RealityKit

extension Entity {
    /*
    Wrap into new entity to avoid multiple
    CollisonComponents inside nested Model
   */
    func wrapped(tag: String = "parent-box") -> ModelEntity {
        let parentEntity = ModelEntity()
        parentEntity.name = tag
        parentEntity.transform = self.transform
        self.setParent(parentEntity, preservingWorldTransform: true)

        let entityBounds = self.visualBounds(relativeTo: parentEntity)
        parentEntity.collision = CollisionComponent(
            shapes: [
                ShapeResource.generateBox(size: entityBounds.extents).offsetBy(
                    translation: entityBounds.center
                )
            ],
            mode: .trigger,
            filter: .sensor
        )

        return parentEntity
    }

    func distance(to otherEntity: Entity) -> Float {
        let translation = self.transformMatrix(relativeTo: nil).columns.3
        let otherTranslation = otherEntity.transformMatrix(relativeTo: nil)
            .columns.3
        let vectorToOtherEntity = otherTranslation - translation
        return length(vectorToOtherEntity)
    }

}
