import Foundation

extension Array {
    /// Returns the element at the specified index if it is within bounds, otherwise nil.
    public subscript(safeIndex index: Int) -> Element? {
        guard index >= 0, index < endIndex else {
            return nil
        }
        return self[index]
    }
}
