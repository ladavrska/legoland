import Combine
import ComposableArchitecture

extension PassthroughSubject: Equatable where Output == [RenderableObject] {
    public static func == (
        lhs: PassthroughSubject<Output, Failure>,
        rhs: PassthroughSubject<Output, Failure>
    ) -> Bool {
        return true
    }
}

extension Publisher where Failure == Error {
    func catchToResult() -> AnyPublisher<Result<Output, Failure>, Never> {
        self
            .map { Result<Output, Failure>.success($0) }
            .catch { error -> AnyPublisher<Result<Output, Failure>, Never> in
                return Just(.failure(error)).eraseToAnyPublisher()
            }
            .eraseToAnyPublisher()
    }
}
