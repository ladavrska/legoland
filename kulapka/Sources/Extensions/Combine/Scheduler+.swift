import Combine
import Foundation

extension Scheduler where Self == DispatchQueue {
    static var main: Self {
        return DispatchQueue.main
    }
}
