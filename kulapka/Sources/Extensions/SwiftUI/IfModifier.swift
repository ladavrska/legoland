import SwiftUI

extension View {
    /// Closure given view if conditional.
    /// - Parameters:
    ///   - conditional: Boolean condition.
    ///   - content: Closure to run on view.
    ///
    /// ```
    /// Text("foo")
    ///  .if(x == 2) {
    ///    $0.bold()
    ///  }
    /// ```
    @ViewBuilder func `if`<Content: View>(
        _ conditional: Bool,
        @ViewBuilder _ content: (Self) -> Content
    ) -> some View {
        if conditional {
            content(self)
        } else {
            self
        }
    }

    /// Closure given view if conditional.
    /// - Parameters:
    ///   - conditional: Boolean condition.
    ///   - truthy: Closure to run on view if true.
    ///   - falsy: Closure to run on view if false.
    ///
    /// ```
    /// Text("georgegarside.com")
    ///  .if(x == 73) {
    ///    $0.bold()
    ///  } else: {
    ///    $0.italic()
    ///  }
    /// ```
    @ViewBuilder func `if`<Truthy: View, Falsy: View>(
        _ conditional: Bool = true,
        @ViewBuilder _ truthy: (Self) -> Truthy,
        @ViewBuilder else falsy: (Self) -> Falsy
    ) -> some View {
        if conditional {
            truthy(self)
        } else {
            falsy(self)
        }
    }

    /// Closure given view and unwrapped optional value if optional is set.
    /// - Parameters:
    ///   - conditional: Optional value.
    ///   - content: Closure to run on view with unwrapped optional.
    ///
    /// ```
    /// Text("@grgarside on Twitter")
    ///  .iflet(socialFont) {
    ///    $0.font($1)
    /// }
    /// ```
    @ViewBuilder func iflet<Content: View, T>(
        _ conditional: T?,
        @ViewBuilder _ content: (Self, _ value: T) -> Content
    ) -> some View {
        if let value = conditional {
            content(self, value)
        } else {
            self
        }
    }
}
