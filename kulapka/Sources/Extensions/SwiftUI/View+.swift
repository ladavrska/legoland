import SwiftUI

extension View {
    public func toAnyView() -> AnyView {
        AnyView(self)
    }
}

extension CGRect {
    static var screenRect: CGRect {
        return .init(
            x: 0,
            y: 0,
            width: UIScreen.main.bounds.width,
            height: UIScreen.main.bounds.height
        )
    }
}
