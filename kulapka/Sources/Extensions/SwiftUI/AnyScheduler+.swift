import ComposableArchitecture

extension AnySchedulerOf {

    static var main: AnySchedulerOf<DispatchQueue> {
        return DispatchQueue.main.eraseToAnyScheduler()
    }

}
