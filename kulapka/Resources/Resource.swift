import ARKit
import Foundation
import RealityKit
import SwiftUI

enum Resource {
    enum Text: String, LocalizedEnum {
        static let prefix: String = "kul."
        case app

        enum Onboarding: String, LocalizedEnum {
            static let prefix: String = Text.prefix + "onboarding."
            case greeting
            case selectAvatar
            case gameSubtitle
            case startButtonTitle
            case avatarCompliment
            case readyToPlay
        }

        enum Setting: String, LocalizedEnum {
            static let prefix: String = Text.prefix + "setting."

            case title
            case language
            case languageCs
            case languageEn
            case shipSetting
            case crew
            case landingDestination
            case personSingle
            case personGroup
            case personSingleNamePlaceholder
            case personGroupNamePlaceholder
        }

        enum LandingDestination: String, LocalizedEnum {
            static let prefix: String = Text.prefix + "landingDestination."
            case prague
            case bratislav
            case zlin
            case bucharest
            case hradecKralove
        }

        enum AR: String, LocalizedEnum {
            static let prefix: String = Text.prefix + "ar."
            case waitingCameraPermission
            case settingUpAR

            enum GameStep: String, LocalizedEnum {
                static let prefix: String = Text.AR.prefix + "gameStep."

                case storyDone = "storyTelling.done"
                case storySkip = "storyTelling.skip"
                case requirePlaneSpaceInstruction =
                    "requirePlane.emptySpaceRequired"
                case requireImageInstruction = "requireImage.instruction"
                case requirePlaneSpaceProportions =
                    "requirePlane.emptySpaceProportions"
                case requireObjectInstruction = "requireObject.instruction"
            }

            enum RecognizableImage: String, LocalizedEnum {
                static let prefix: String =
                    Text.AR.prefix + "recognizableImage."

                case books
                case reception
                case earth
                case bottle
                case drink
                case itroom
            }

            enum RecognizableObject: String, LocalizedEnum {
                static let prefix: String = AR.prefix + "recognizableObject."
                case printer
                case coffeeMachine
            }
        }

        enum StoryText: LocalizedEnum {
            static let prefix: String = Text.prefix + "storyText."

            case storyBigMeetingRoomName
            case storySocialRoomName
            case legend(PersonSetting)
            case lookForBottle(PersonSetting)
            case foundBottle(PersonSetting)
            case foundBottleConfirm
            case forDrinkGoFloor1
            case forDrinkGoFloor1Confirm
            case forDrinkGoSocialRoom
            case forITSymbolGoITRoom
            case forITSymbolConfirm
            case foundITSymbol(PersonSetting)
            case foundITSymbolConfirm

            init?(rawValue: String) { fatalError() }

            var rawValue: String {
                switch self {
                case .storyBigMeetingRoomName:
                    return "storyBigMeetingRoomName"
                case .storySocialRoomName:
                    return "storySocialRoomName"
                case .legend(let person):
                    return "legend." + person.rawValue
                case .lookForBottle(let person):
                    return "lookForBottle." + person.rawValue
                case .foundBottle(let person):
                    return "foundBottle." + person.rawValue
                case .foundBottleConfirm:
                    return "foundBottleConfirm"
                case .forDrinkGoFloor1:
                    return "forDrinkGoFloor1"
                case .forDrinkGoFloor1Confirm:
                    return "forDrinkGoFloor1Confirm"
                case .forDrinkGoSocialRoom:
                    return "forDrinkGoSocialRoom"
                case .forITSymbolGoITRoom:
                    return "forITSymbolGoITRoom"
                case .forITSymbolConfirm:
                    return "forITSymbolConfirm"
                case .foundITSymbol(let person):
                    return "foundITSymbol." + person.rawValue
                case .foundITSymbolConfirm:
                    return "foundITSymbolConfirm"
                }
            }

            enum BigMeetingRoom: String, LocalizedEnum {
                static let prefix: String = StoryText.prefix + "bigMeetingRoom."

                case gratulationFinishedStoryTitle
                case gratulationFinishedStoryDescription
                case gratulationFinishedStoryButtonTitle
            }

            enum SocialRoom: String, LocalizedEnum {
                static let prefix: String = StoryText.prefix + "socialRoom."

                case firstInstructionsText
                case firstInstructionsButtonTitle
                case requireCoffeeMachineObjectDetectInstruction
                case stepDragdrawerFullTitle
                case stepDragdrawerFullText
                case stepRequireCupTitle
                case stepRequireCupText
                case stepPressCoffeeButtonText
                case coffeePreparationDescription
                case coffeeDoneInstruction
                case stepRequireOrangeTitle
                case stepRequireOrangeText
                case stepCheckFridgeTitle
                case stepCheckFridgeText
                case stepPlayPlaystationText
                case lastInstructionsText
                case gratulationFinishedSceneTitle
                case gratulationFinishedSceneDescription
                case gratulationFinishedSceneButtonTitle
            }

            enum Warehouse: String, LocalizedEnum {
                static let prefix: String = "warehouse."

                case blank  // TODO: Delete this
            }

            enum ITRoom: String, LocalizedEnum {
                static let prefix: String = StoryText.prefix + "itRoom."

                case stepInstructions
                case stepShootMonkeySuccess
                case stepShootMonkeyFailure
                case stepShootingCompleted
                case gratulationFinishedSceneTitle
                case gratulationFinishedSceneDescription
                case gratulationFinishedSceneButtonTitle
            }
        }
    }

    enum Image: String {
        case ar_glyph_dark
        case ar_glyph_light
        case book = "ar_books"
        case earth = "ar_earth"
        case reception = "ar_reception"
        case pirate
        case pirate_welcome
        case settings
        case pirate_bottle
        case pirate_bartender
        case pirate_map
        case ar_bottle
        case ar_drink
        case background
        case message
        case vector
        case girl_warehouse
        case printer_preview
        case itroom = "ar_it_room"

        // MARK: - Icons

        case pirate_back

        // MARK: Avatars
        case captain_pirate_hat =
            "captain_pirate_hat_character_people_vintage_beard_corsair_icon_131608"
        case gold_money_coin =
            "gold_money_coin_treasure_pirate_golden_skull_icon_131578"
        case gun_pirate_pistol =
            "gun_pirate_pistol_vintage_corsair_caribbean_crew_icon_131572"
        case pirate_costume_woman =
            "pirate_costume_woman_female_sexy_sword_person_sailor_lady_icon_131574"
        case pirate_skull_skeleton =
            "pirate_skull_skeleton_danger_head_bone_dead_crossbones_horror_icon_131575"
        case sailor_sea_captain =
            "sailor_sea_captain_navy_seaman_hat_adventure_vintage_icon_131583"
        case shippirate_sea_boat =
            "shippirate_sea_boat_ship_sail_ocean_old_adventure_wooden_flag_icon_131605"
        case sword_pirate_saber =
            "sword_pirate_saber_blade_crew_corsair_icon_131609"
        case treasure_gold_chest =
            "treasure_gold_chest_old_box_pirate_ancient_antique_icon_131607"
        case treasure_old_vintage =
            "treasure_old_vintage_map_paper_antique_compass_pirate_adventure_travel_icon_131600"
        case wheel_ship_sea =
            "wheel_ship_sea_nautical_control_boat_steering_icon_131577"

        // Maps
        case nomap
        case smeckyLevel0
        case smeckyLevel1
        case smecky_floor1_go_social
        case smecky_floor2_go_floor1
        case smecky_floor0_go_floor1

        var image: SwiftUI.Image {
            SwiftUI.Image(self.rawValue, bundle: .main)
        }

        var uiImage: UIImage? {
            UIImage(named: self.rawValue, in: .main, compatibleWith: nil)
        }
    }

    enum Color: String {

        case blackish = "Blackish"
        case selected
        case primary
        case disabled  // #5B5B5B
        case disabled_bg  // #BABABA

        public var color: SwiftUI.Color {
            return SwiftUI.Color(self.rawValue, bundle: .main)
        }

        public var uiColor: UIColor {
            return UIColor(color)
        }
    }

    enum USDZFile: String {
        case task
        case earth
        case books

        var type: String {
            return "usdz"
        }

        func loadEntity() throws -> Entity? {
            return try Entity.load(named: self.rawValue, in: .main)
        }

        func loadAsync() throws -> LoadRequest<Entity> {
            return Entity.loadAsync(named: self.rawValue, in: .main)
        }
    }

}

protocol LocalizedEnum: RawRepresentable where RawValue == String {
    static var prefix: String { get }
}

extension LocalizedEnum {
    var localized: String {
        self.key.localized
    }

    private var key: String {
        return Self.prefix + self.rawValue
    }
}
