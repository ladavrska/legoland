#  Kulapka

## Instalation

```bash
./bootstrap.sh
```

### Open 

```
open ./kulapka.xcodeproj
```

### Tools

- **Code Format** (using `swift-format`):
  - run `./scripts/format.sh`
